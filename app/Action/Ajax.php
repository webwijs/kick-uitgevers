<?php

namespace Theme\Action;

use Webwijs\Http\Request;
use Webwijs\View;

use Theme\Media\FileHandler;

class Ajax
{
    /**
     * Displays a single list item for a sortable list of processes.
     *
     * @return void
     */
    public static function loadSortableItem()
    {
        $request = new Request();
        switch ($request->getMethod()) {
            case 'POST':
                $postId = $request->getPost('post_id');
                $name = $request->getPost('name', '');
                break;
            case 'GET':
            default:
                $postId = $request->getQuery('post_id');
                $name = $request->getQuery('name', '');
                break;
        }
        
        $view = new View();   
        if ($postId) {
            $post = get_post($postId);
            if (is_object($post)) {
                echo $view->partial('partials/metaboxes/generic/list-item.phtml', array('post' => $post, 'name' => $name));
            }
        }
        exit();
    }

    public static function loadItems()
    {
        $request = new Request();
        
        switch ($request->getMethod()) {
            case 'POST':
                $value = $request->getPost('content', '');
                $type = $request->getPost('type', '');
                break;
            case 'GET':
            default:
                $value = $request->getQuery('content', '');
                $type = $request->getQuery('type', '');
                break;
        }

        // var_dump(json_decode(urldecode($value)));
        // exit;

        $view = new View();
        if ($value && $type) {
            echo $view->partial('partials/metaboxes/generic/content-item.phtml', array('content' => $value, 'type' => $type));
        }
        exit();
    }

    /**
     * Moves file to the upload directory and store a new record in the database.
     */
    public static function uploadFile()
    {


        if (isset($_FILES['userfile'])) {
            $file = $_FILES['userfile'];
            $media = new FileHandler();
            
            // return error message if file type is not allowed.
            if (!$media->isValid($file)) {
                echo json_encode(array('message' => __('Dit bestand is niet toegestaan'), 'code' => 'FileTypeNotAllowed'));
                exit;
            }

            // move uploaded file to new upload directory.
            if ($filename = $media->moveFile($file)) {                  
                echo json_encode(array('filename' => $filename, 'code' => 'UploadSuccess'));
                exit;
            }
        }
    }
    
    /**
     * Delete file from the upload directory and remove associated record from the database.
     */
    public static function deleteFile()
    {
        $request = new Request();
        if ( $filename = $request->getPost('filename')) {             
            // delete file from server.
            $media = new FileHandler();
            if ($media->removeFile($filename)) {
                echo json_encode(array('message' => __('Bestand sucessvol verwijderd'), 'code' => 'DeleteSuccess'));
                exit;
            }
        }

        echo json_encode(array('message' => __('Dit bestand is niet gevonden'), 'code' => 'FileNotFound'));
        exit;
    }
}
