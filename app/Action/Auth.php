<?php

namespace Theme\Action;

use Webwijs\View;

/**
 * The Auth class contains static methods which have been registered with WordPress actions.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class Auth
{    
    /**
     * Always redirect visitors to the authentication page.
     *
     * @return void
     */
    public static function authenticated()
    {
        // skip check for WordPress users.
        if (is_user_logged_in()) {
            return;
        }
        
        // pages that are always accessible.
        $exclude = array(
            get_option('theme_page_user_login'),
            get_option('theme_page_user_logout'),
            get_option('theme_page_user_lost_password'),
            get_option('theme_page_user_reset_password'),
        );
        
        $postId = get_option('theme_page_user_login');
        $post   = $GLOBALS['post'];
        $restricted = get_post_meta($post->ID, '_page_restricted',true);

        if ($postId && $restricted) {
            $view = new View();
            if ($view->moduleActive('auth') && $view->identity() === null) {
                $location = get_permalink($postId);
                if (is_string($location)) {
                    wp_safe_redirect($location, 302);
                    exit();
                }
            }
        }
    }
}
