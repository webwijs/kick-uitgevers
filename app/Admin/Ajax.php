<?php
namespace Theme\Admin;

use Theme\Bootstrap;

use Webwijs\Cache\Loader\FileCacheLoader;


class Ajax
{
    public static function compile()
    {
        Bootstrap::compileSCSS(true);
        exit;
    }

    public static function deleteFileCache()
    {
        $dir = FileCacheLoader::getDefaultDirectory();
        if(is_dir($dir)) {
            $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
            $files = new \RecursiveIteratorIterator($it,
            \RecursiveIteratorIterator::CHILD_FIRST);
            foreach($files as $file) {
              if ($file->isDir()){
                rmdir($file->getRealPath());
              } else {
                unlink($file->getRealPath());
              }
            }
            rmdir($dir);
            exit;
        }
        exit;
    }
}
