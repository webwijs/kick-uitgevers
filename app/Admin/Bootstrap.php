<?php

namespace Theme\Admin;

use Webwijs\AbstractBootstrap;
use Webwijs\Admin\Metabox\PageLayout;
use Webwijs\Admin\Metabox\Excerpt;
use Webwijs\Admin\Metabox\Multibox;

use Theme\Admin\Controller\SettingsController;
use Theme\Admin\Controller\SidebarsController;

use Theme\Admin\Metabox\BlockSelect;

class Bootstrap extends AbstractBootstrap
{
    protected function _initSettings()
    {
        add_action('admin_head', function(){
          wp_enqueue_style('bootstrap', get_template_directory_uri(). '/assets/lib/css/bootstrap.css');
        });
        $builder = SettingsController::getBuilder();

        $builder->group('page', __('Pagina koppelingen'))
                ->add('theme_page_404', 'postSelect', array('label' => __('404 pagina')))
                ->add('theme_page_paymentoptions', 'postSelect', array('label' => __('Betaalopties pagina')));

        $builder->group('hide', 'Verberg elementen');

        $builder->group('form', 'Formulieren')
                ->add('theme_form_reprint', 'formSelect', array('label' => __('Houd me op de hoogte formulier')))
                ->add('theme_form_newsletter', 'formSelect', array('label' => __('Nieuwsbriefformulier')));

        $builder->group('thumbnail', __('Afbeeldingen'))
                ->add('theme_thumbnail_news', 'postSelect', array('label' => __('Standaard nieuws afbeelding'), 'queryArgs' => array('post_types' => array('attachment'))))
                ->add('theme_thumbnail_video', 'postSelect', array('label' => __('Home video afbeelding'), 'queryArgs' => array('post_types' => array('attachment'))));

        $builder->group('video', __('Video'))
                ->add('theme_video_id', 'text', array('label' => __('Video ID (youtube)')));

        $builder->group('advanced', __('Geavanceerd'))
                ->add('theme_advanced_flat_url', 'checkbox', array('label' => __('Platte URL\'s')))
                ->add('theme_advanced_scss_compiler', 'compilerSelect', array('label' => __('SCSS compiler modus')))
                ->add('theme_advanced_compile_scss_now', 'compile', array('label' => __('Compileer SCSS nu')));

        $builder->group('cache', __('Cache'))
                ->add('theme_advanced_varnish', 'checkbox', array('label' => __('Varnish inschakelen')))
                ->add('theme_advanced_redis_cache', 'checkbox', array('label' => __('Redis Cache inschakelen')))
                ->add('theme_advanced_delete_file_cache', 'deleteFileCache', array('label' => __('Verwijder bestandsysteem cache')));

        $builder->group('company', __('Contactgegevens'))
                ->add('theme_company_email', 'text', array('label' => __('E-mailadres')))
                ->add('theme_company_phone', 'text', array('label' => __('Telefoonnummer (algemeen)')))
                ->add('theme_company_commerce_number', 'text', array('label' => __('KVK-nummer')))
                ->add('theme_company_tax_number', 'text', array('label' => __('BTW-nummer')))
                ->add('theme_company_iban_number', 'text', array('label' => __('IBAN-nummer')))
                ->add('theme_company_bic_number', 'text', array('label' => __('BIC-nummer')))
                ->add('theme_company_address', 'text', array('type' => 'textarea', 'label' => __('Vestigingsplaats')));

        new SettingsController();
    }

    protected function _initSidebars()
    {
        $sidebarsController = new SidebarsController();
    }

    protected function _initRoles()
    {
        add_filter('editable_roles', array('Theme\Admin\Filter\User', 'editableRoles'));
    }

    protected function _initEditor()
    {
        //add_filter('mce_external_plugins', array('Theme\Admin\Filter\TinyMCE', 'addButtonPlugin'));
        //add_filter('mce_buttons_2', array('Theme\Admin\Filter\TinyMCE', 'registerButtons'));

        add_filter('mce_buttons_2', array('Theme\Admin\Filter\TinyMCE', 'styleSelect'));
        add_filter('tiny_mce_before_init', array('Theme\Admin\Filter\TinyMCE', 'stylesDropdown'));

        add_action('admin_init', array('Theme\Admin\Filter\TinyMCE', 'enqueueStyles'));
        add_action('admin_head', array('Theme\Admin\Filter\TinyMCE', 'editorStyle'));
        add_filter('tiny_mce_before_init', array('Theme\Admin\Filter\TinyMCE', 'editorInit'));
    }

    protected function _initAdminLayout()
    {
        add_action('admin_head', array('Theme\Admin\Action\AdminLayout', 'menuLayout'));
        add_action('admin_menu', array('Theme\Admin\Action\AdminLayout', 'menuItems'));
        add_action('wp_dashboard_setup', array('Theme\Admin\Action\AdminLayout', 'removeWidgets'));
        add_action('add_meta_boxes', array('Theme\Admin\Action\AdminLayout', 'removeMetaBoxes'));
        add_action('add_meta_boxes', array('Theme\Admin\Action\YoastSeo', 'lessIntrusive'));
        add_action('add_meta_boxes', array('Theme\Admin\Action\AdminLayout', 'removeMetaBoxes'));
        add_filter('manage_posts_columns', array('Theme\Admin\Action\AdminLayout', 'listTableColumns'), 10, 2);
        add_filter('manage_pages_columns', array('Theme\Admin\Action\AdminLayout', 'listTableColumns'), 10, 2);
        foreach (get_post_types() as $post_type) {
            add_filter('get_user_option_closedpostboxes_' . $post_type, array('Theme\Admin\Action\AdminLayout', 'closedMetaBoxes'));
            add_filter('get_user_option_metaboxhidden_' . $post_type, array('Theme\Admin\Action\AdminLayout', 'hiddenMetaBoxes'));
        }
    }

    protected function _initPages()
    {
        Multibox::register('page', array('id' => 'settings', 'title' => 'Instellingen', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Visibility'),
            array('class' => 'Webwijs\Admin\Metabox\ParentPage'),
            array('class' => 'Webwijs\Admin\Metabox\MenuOrder')
        )));

        PageLayout::register('page');
        Excerpt::register('page');

        Multibox::register('page', array('id' => 'texts', 'context'=>'normal', 'title' => 'Extra teksten', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Text', 'settings' => array('id' => 'text_toptitle', 'title' => 'Pagina titel (h1)')),
            array('class' => 'Webwijs\Admin\Metabox\Text', 'settings' => array('id' => 'text_videotitle', 'title' => 'Video titel')),
            array('class' => 'Webwijs\Admin\Metabox\Text', 'settings' => array('id' => 'text_videosubtitle', 'title' => 'Video subtitel'))
        )));
    }

    protected function _initNews()
    {
        PageLayout::register('news');
        Excerpt::register('news');
    }

    protected function _initProduct()
    {    
        BlockSelect::register('product', array('id'=>'product_usps', 'title' => 'Product USPs contentblock'));
        Multibox::register('product', array('id' => 'texts', 'context'=>'side', 'title' => 'Extra teksten', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Text', 'settings' => array('id' => 'product_preordertext', 'title' => 'Pre-order tekst')),
        )));
    }
}
