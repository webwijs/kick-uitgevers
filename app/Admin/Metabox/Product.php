<?php

namespace Theme\Admin\Metabox;

use Webwijs\Admin\AbstractMetabox;
use Webwijs\Post;
use Webwijs\View;

class Product extends AbstractMetabox
{    
    /**
     * settings used by the metabox.
     *
     * @var array
     */
    protected $settings = array(
        'id'       => 'products',
        'title'    => 'Producten',
        'context'  => 'normal',
        'priority' => 'high',
    );
    
    /**
     * The post types.
     *
     * @var array
     */ 
    public $postTypes = array('product');
    
    /**
     * The relation key
     *
     * @var string
     */
    private $relationKey = 'selected_product';
    
    /**
     * Initialize metabox.
     */
    public function init()
    {
	    add_action('admin_enqueue_scripts', array($this, 'enqueueScripts'));
    }
    
    /**
     * Display a form or other html elements which can be used associate meta data with a particular post.
     *
     * @param WP_Post $post the post object which is currently being displayed.
     */
    public function display($post)
    { 
        $view = new View();

        $posts = $this->getRelatedProcesses($post);
    ?>
        <div class="sortable-posts-container">
            <p>Selecteer hieronder een product en klik vervolgens op de knop toevoegen. Sleep vervolgens de objecten in de volgorde die je wenst.</p>
            <ol class="post-list">
                <?php foreach($posts as $post): ?>
                    <?php echo $view->partial('partials/metaboxes/generic/list-item.phtml', array('post' => $post, 'name' => $this->getName('post_id'))) ?>
                <?php endforeach ?>
            </ol>

            <div class="post-select-container table">
                <div class="column">
                    <?php echo $view->dropdownPosts(array(
                        'post_types' => $this->getPostTypes(),
                        'name'       => $this->getName(''),
                        'class'      => 'post-select',
                    )) ?>
                </div>
                <div class="column">
                    <input type="button" data-name="<?php echo $this->getName('post_id') ?>" name="add-button" value="<?php _e('Add'); ?>" class="button" />
                </div>
            </div>
        </div>
    <?php
    }
    
    /**
     * Allows the meta data entered on the admin page to be saved with a particular post.
     *
     * @param int $postId the ID of the post that the user is editing.
     */
    public function save($postId)
    {
        $postIds = (array) $this->getPostValue('post_id');
        Post::updateRelatedPosts($postId, $postIds, $this->getRelationKey());
    }
    
    /**
     * returns all processes associated with the given post.
     *
     * @param WP_Post $post the post for which to retrieve processes.
     * @return array tabs associated with the current post.
     */
    protected function getRelatedProcesses($post = null)
    {
        $processes = array();
        
        $postIds = Post::getRelatedPostIds($this->getRelationKey(), $post);
        if (!empty($postIds)) {
            $processes = get_posts(array(
                'post__in'  => $postIds,
                'post_type' => $this->getPostTypes(),
                'orderby'   => 'post__in',
                'order'     => 'asc',
                'nopaging' => true
            ));
        }
        
        return $processes;
    }
    
    /**
     * Set the post type.
     *
     * @param array $postTypes the post type.
     * @throws InvalidArgumentException if the given argument is not an array.
     */
    public function setPostTypes(array $postTypes)
    {        
        $this->postTypes = (array) $postTypes;
    }
    
    /**
     * Returns the post type.
     *
     * @return string the post type.
     */
    public function getPostTypes()
    {
        return $this->postTypes;
    }
    
    /**
     * Set the relation key.
     *
     * @param string $relationKey the relation key.
     * @throws InvalidArgumentException if the given argument if not of type 'string'.
     */
    public function setRelationKey($relationKey)
    {
        if (!is_string($relationKey)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($relationKey) ? get_class($relationKey) : gettype($relationKey))
            ));
        }
        
        $this->relationKey = $relationKey;
    }
    
    /**
     * Returns the relation key.
     *
     * @return string the relation key.
     */
    public function getRelationKey()
    {
        return $this->relationKey;
    }
    
	/**
	 * Loads all the necessary scripts which allows this metabox to behave and display normal.
	 *
	 * @param string $hook name of the currently active page.
	 */
	public function enqueueScripts($hook) {	
        wp_enqueue_script('jquery-sortable-processes', get_bloginfo('stylesheet_directory') . '/assets/lib/js/admin/jquery.sortable-processes.js', array('jquery', 'jquery-ui-sortable'));
        wp_enqueue_style('processes-admin-style', get_bloginfo('stylesheet_directory') . '/assets/lib/css/admin.css');
	}
}
