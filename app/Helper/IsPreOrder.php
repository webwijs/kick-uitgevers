<?php

namespace Theme\Helper;


class IsPreOrder
{

    public function isPreOrder(){
      
        global $product;
        $status = wc_get_product_terms( $product->get_id(), 'pa_status', array( 'fields' => 'slugs' ) );

        $preorder = false;
        if(in_array('pre-order', $status)){
            $preorder = true;
        }

        return $preorder;

    }

}