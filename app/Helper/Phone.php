<?php

namespace Theme\Helper;

use Theme\System\Device;

use Webwijs\Http\Request;

class Phone
{
    
    /**
     * Returns an HTML representation of the given telephone number. If the given device is a mobile phone
     * the given telephone number will be prepended with the tel URI scheme.
     *
     * @param mixed $number the telephone number to display.
     * @param array|null $args (optional) change what is returned by this helper.
     * @return string a string containing HTML elements to display the telephone number on a webpage.
     */
    public function phone($number, $location = 'normal', $args = null)
    {
        $defaults = array(
            'container'       => 'span',
            'container_class' => 'phone-container',
            'class'           => 'phone',
        );
        $args = array_merge($defaults, (array) $args);
        
        $device = new Device(new Request());
        if ( ($device->isPhone() || $device->isSmartphone()) && $location == 'header') {
            $output = sprintf('<a class="%s" href="tel:%s">%s</a>', esc_attr($args['class']), $this->sanitizePhoneNumber($number), esc_html($number));
        } 
        else if( ($device->isPhone() || $device->isSmartphone()) && $location == 'normal'){
            $output = sprintf('<a class="%s" href="tel:%s">%s</a>', esc_attr($args['class']), $this->sanitizePhoneNumber($number), esc_html($number));
        }
        else {
            $output = sprintf('<span class="%s"><span>%s</span></span>', esc_attr($args['class']), esc_html($number)); 
        }

        if (!empty($args['container'])) {
            $output = sprintf('<%1$s class="%2$s">%3$s</%1$s>', esc_attr($args['container']), esc_attr($args['container_class']), $output);
        }
        
        return $output;
    }
    
    /**
     * Returns a sanitized phone number which can be used in conjunction with the tel URI scheme.
     *
     * @param mixed $number the telephone number to sanitize.
     * @return string a sanitized phone number (i.e. +31201234567)
     * @throws InvalidArgumentException if the given argument is not a scalar value.
     * @see http://php.net/manual/en/function.is-scalar.php PHP scalar types.
     */
    private function sanitizePhoneNumber($number)
    {
        if (!is_scalar($number)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a scalar value as argument; received "%s"',
                __METHOD__,
                (is_object($number)) ? get_class($number) : gettype($number)
            ));
        }
        
        return preg_replace('#(?<!^)+[^\d]+#', '', trim($number));
    }
}
