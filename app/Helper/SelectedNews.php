<?php

namespace Theme\Helper;

use Webwijs\Post;

class SelectedNews
{
    public function selectedNews($args = null, $post = null)
    {
        if ($post === null) {
            $post = $GLOBALS['post'];
        }
        
        $defaults = array(
            'relation_key' => 'selected_news',
            'template'     => 'partials/news/selected-list.phtml',
            'vars'         => null,
        );
        $args = array_merge($defaults, (array) $args);
        
        $posts = array();
        
        $postIds = Post::getRelatedPostIds($args['relation_key'], $post);

        if (!empty($postIds)) {
            $posts = get_posts(array(
                'post__in'  => $postIds,
                'post_type' => 'news',
                'orderby'   => 'post__in',
                'order'     => 'asc',
                'posts_per_page' => -1,
                'nopaging'  => true,
            ));
        }

        return $this->view->partial($args['template'], array_merge((array) $args['vars'], array('posts' => $posts)));
    }
}
