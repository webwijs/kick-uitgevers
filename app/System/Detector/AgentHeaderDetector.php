<?php

namespace Theme\System\Detector;

use Theme\System\Device;

use Webwijs\Http\Request;

/**
 * A detector that determines the device based on the browser's user agent.
 *
 * @author Chris Harris
 * @version 0.0.9
 */
class AgentHeaderDetector extends AbstractDetector
{
    /**
     * A collection of user agents for a smartphone.
     *
     * @var array
     */
    private $smartphone = array(
        'iphone', 'ipod', 'android', 'opera mini', 'blackberry',
        'series60|series 60', // symbian
        'pre\/|palm os|palm|hiptop|avantgo|plucker|xiino|blazer|elaine', // palm
        'iris|3g_t|windows ce|opera mobi|iemobile', // windows
        'maemo|tablet|qt embedded|com2', // nokia
    );
    
    /**
     * A collection of user agents for a phone.
     *
     * @var array
     */
    private $phone = array(
                'mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|m881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|s800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|d736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |sonyericsson|samsung|240x|x320|vx10|nokia|sony cmd|motorola|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|psp|treo|vnd.rim|wml|nitro|nintendo|wii|xbox|archos|openweb|mini|docomo',
                '^1207|^3gso|^4thp|^501i|^502i|^503i|^504i|^505i|^506i|^6310|^6590|^770s|^802s',
                '^abac|^acer|^acoo|^acs-|^aiko|^airn|^alav|^alca|^alco|^amoi|^anex|^anny|^anyw|^aptu|^arch|^argo|^aste|^asus|^attw|^audi|^au-m|^aur |^aus |^avan|^a wa',
                '^beck|^bell|^benq|^benq|^bilb|^bird|^blac|^blaz|^brew|^brvw|^bumb|^bw-n|^bw-u',
                '^c55\/|^capi|^ccwa|^cdm-|^cell|^chtm|^cldc|^cmd-|^cond|^craw',
                '^dait|^dall|^dang|^dbte|^dc-s|^devi|^dica|^dmob|^doco|^dopo|^ds12|^ds-d',
                '^el49|^elai|^eml2|^emul|^eric|^erk0|^esl8|^ez40|^ez60|^ez70|^ezos|^ezwa|^ezze',
                '^fake|^fetc|^fly_|^fly-',
                '^g1 u|^g560|^gene|^gf-5|^g-mo|^good|^go.w|^grad|^grun',
                '^haie|^hcit|^hd-m|^hd-p|^hd-t|^hei-|^hiba|^hipt|^hita|^hp i|^hpip|^hs-c|^htc |^htc_|^htc-|^htca|^htcg|^htcp|^htcs|^htct|^http|^huaw|^hutc',
                '^i-20|^i230|^iac|^iac-|^iac\/|^ibro|^idea|^ig01|^i-go|^ikom|^im1k|^i-ma|^inno|^ipaq|^iris',
                '^jata|^java|^jbro|^jemu|^jigs',
                '^kddi|^keji|^kgt|^kgt\/|^klon|^kpt |^kwc-|^kyoc|^kyok',
                '^leno|^lexi|^lg-|^lg50|^lg54|^lge-|^lge\/|^lg g|^lg\/k|^lg\/l|^lg\/u|^libw|^lynx',
                '^m1-w|^m3ga|^m50\/|^mate|^maui|^maxo|^mc01|^mc21|^mcca|^m-cr|^medi|^merc|^meri|^midp|^mio8|^mioa|^mits|^mits|^mmef|^mo01|^mo02|^mobi|^mode|^modo|^mot |^mot-|^moto|^motv|^mozz|^mt50|^mtp1|^mtv |^mwbp|^mywa',
                '^n100|^n101|^n102|^n202|^n203|^n300|^n302|^n500|^n502|^n505|^n700|^n701|^n710|^nec-|^nem-|^neon|^netf|^newg|^newt|^nok6|^noki|^nzph',
                '^o2im|^o2 x|^o2-x|^opti|^opwv|^oran|^owg1',
                '^p800|^palm|^pana|^pand|^pant|^pdxg|^pg-1|^pg13|^pg-2|^pg-3|^pg-6|^pg-8|^pg-c|^phil|^pire|^play|^pluc|^pn-2|^pock|^port|^pose|^prox|^psio|^pt-g',
                '^qa-a|^qc07|^qc12|^qc-2|^qc21|^qc-3|^qc32|^qc-5|^qc60|^qc-7|^qci-|^qtek|^qwap',
                '^r380|^r600|^raks|^rim9|^rove|^rozo',
                '^s55\/|^sage|^sama|^samm|^sams|^sany|^sava|^sc01|^sch-|^sch-|^scoo|^scp-|^sdk\/|^se47|^sec-|^sec0|^sec1|^semc|^send|^seri|^sgh-|^shar|^shar|^sie-|^siem|^sk-0|^sl45|^slid|^smal|^smar|^smb3|^smit|^smt5|^soft|^sony|^sp01|^sph-|^spv |^spv-|^sy01|^symb',
                '^t218|^t250|^t600|^t610|^t618|^tagt|^talk|^tcl-|^tdg-|^teli|^telm|^tim-|^t-mo|^topl|^tosh|^treo|^ts70|^tsm-|^tsm3|^tsm5|^tx-9',
                '^up.b|^upg1|^upsi|^utst',
                '^v400|^v750|^veri|^virg|^vite|^vk40|^vk50|^vk52|^vk53|^vk-v|^vm40|^voda|^vulc|^vx52|^vx53|^vx60|^vx61|^vx70|^vx80|^vx81|^vx83|^vx85|^vx98',
                '^w3c |^w3c-|^wap-|^wapa|^wapi|^wapj|^wapm|^wapp|^wapr|^waps|^wapt|^wapu|^wapv|^wapy|^webc|^whit|^wig |^winc|^winw|^wml|^wonu',
                '^x700|^xda-|^xda2|^xdag',
                '^yas-|^your',
                '^zeto|^zte-',
    );

    /**
     * {@inheritDoc}
     */
    public function detect(Request $request)
    {
        $detected = -1;
        if (preg_match($this->getSmartphoneRegex(), $request->getServer('HTTP_USER_AGENT', ''))) {
            $detected = Device::SMARTPHONE;
        } else if (preg_match($this->getPhoneRegex(), $request->getServer('HTTP_USER_AGENT', ''))) {
           $detected = Device::PHONE;
        } else if ($this->detector !== null) {
            $detected = $this->detector->detect($request);
        }
        
        return $detected;
    }

    /**
     * Returns a regular expression to detect the user agent of a smartphone.
     *
     * @return string a regular expression.
     */
    private function getSmartphoneRegex()
    {
        return sprintf('#%1$s#i', implode('|', $this->smartphone));
    }
    
    /**
     * Returns a regular expression to detect the user agent of a phone.
     *
     * @return string a regular expression.
     */
    private function getPhoneRegex()
    {
        return sprintf('"#%1$s#i"', implode('|', $this->phone));
    }
}
