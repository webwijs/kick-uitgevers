<?php

namespace Theme\System;

use Webwijs\Http\Request;

use Theme\System\Detector\DetectorInterface;
use Theme\System\Detector\AgentHeaderDetector;
use Theme\System\Detector\AcceptHeaderDetector;
use Theme\System\Detector\XHeaderDetector;

/**
 * Detects what device was used to request a page.
 *
 * @author Chris Harris
 * @version 0.0.9
 */
class Device
{
    /**
     * Desktop device.
     *
     * @var int
     */
    const DESKTOP = 0;
    
    /**
     * Smartphone device.
     *
     * @var int
     */
    const SMARTPHONE = 1;
    
    /**
     * Phone device.
     *
     * @var int
     */
    const PHONE = 2;
    
    /**
     * The device type
     *
     * @var int
     */
    private $deviceType;
    
    /**
     * A HTTP request object.
     *
     * @var Request
     */
    private $request;
    
    /**
     * A device detector. 
     *
     * @var DetectorInterface
     */
    private $detector;
    
    /**
     * Construct a new Device.
     *
     * @param Request $request the HTTP request.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->setupDetectors();
    }
    
    /**
     * Returns true for a smartphone device.
     *
     * @return bool true if a smartphone device is used, false otherwise.
     */
    public function isSmartphone()
    {
        if ($this->deviceType === null) {
            $this->detect();
        }
        
        return ($this->deviceType === self::SMARTPHONE);
    }
    
    /**
     * Returns true for a phone device.
     *
     * @return bool true if a phone device is used, false otherwise.
     */
    public function isPhone()
    {
        if ($this->deviceType === null) {
            $this->detect();
        }
        
        return ($this->deviceType === self::PHONE);
    }
    
    /**
     * Returns true for a desktop device.
     *
     * @return bool true if a desktop device is used, false otherwise.
     */
    public function isDesktop()
    {
        if ($this->deviceType === null) {
            $this->detect();
        }
        
        return ($this->deviceType === self::DESKTOP);
    }
    
    /**
     * Clear any previous detected device type.
     *
     * @return bool true if a previous device type was removed, false otherwise.
     */
    public function reset()
    {
        $retval = false;
        if ($retval = ($this->deviceType !== null)) {
            $this->deviceType = null;
        }
        
        return $retval;
    }
    
    /**
     * Add a new detector.
     *
     * @param DetectorInterface $detector the detector to add.
     */
    public function addDetector($detector)
    {
        if (($nextDetector = $this->getDetector()) !== null) {
            $detector->setNextDetector($nextDetector);
        }
        $this->setDetector($detector);
    }
    
    /**
     * Set the detector.
     *
     * @param DetectorInterface $detector the detector to set.
     */
    public function setDetector($detector)
    {
        $this->detector = $detector;
    }
    
    /**
     * Returns the detector.
     *
     * @return DetectorInterface|null the detector, or null if no detector is available.
     */
    public function getDetector()
    {
        return $this->detector;
    }
    
    /**
     * Returns true if at least one detector is used to detect the device.
     *
     * @return bool true if a detector is present, false otherwise.
     */
    public function hasDetectors()
    {
        return ($this->getDetector() !== null);
    }
    
    /**
     * Removes detector(s) that determines the device type of the client.
     *
     * @return void
     */
    public function clearDetectors()
    {
        $this->detector = null;
    }

    /**
     * Detects what device type the client is using.
     *
     * @return void.
     */
    private function detect()
    {
        $deviceType = self::DESKTOP;
        if (($detector = $this->getDetector()) !== null) {
            $detectedType = $detector->detect($this->request);
            if ($detectedType > -1) {
                $deviceType = $detectedType; 
            }
        }
        
        $this->deviceType = $deviceType;
    }
    
    /**
     * Creates a chain of detectors.
     *
     * @return void.
     */
    private function setupDetectors()
    {
        $this->setDetector(new XHeaderDetector());
        $this->addDetector(new AcceptHeaderDetector());
        $this->addDetector(new AgentHeaderDetector());
    }
}
