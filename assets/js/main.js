// jQuery(window).load(function($){
//     jQuery('#newslist').masonry({
//         itemSelector: '#newslist .column'
//     });
// });

jQuery(document).ready(function($) {

    $(document).on('click', '.toggle', function(e) {
        e.preventDefault();
        var self = $(this);
        var target = $(self.attr('href'));
        target.toggleClass('active');
        self.toggleClass('activated');
        return false;
    });

    $(document).on('click', '.scrollto', function(e) {
        e.preventDefault();
        var self = $(this);
        var target = $(self.data('target'));
        $('html,body').animate({
           scrollTop: target.offset().top
        });
        return false;
    });

    $('.content-header').slick({
        'arrows': false,
        'dots': true,
        'autoplay':true,
        'autoplaySpeed':4000,
        'speed': 1500,
    });

    $('.content-soon').slick({
        'arrows': false,
        'dots': true,
        'autoplay':true,
        'autoplaySpeed':3000,
    });

    $('.content-top3').slick({
        'arrows': false,
        'dots': true,
        'slidesToShow': 1,
        'slidesToScroll': 1,
        'autoplay':true,
        'autoplaySpeed':3000,
        'responsive': [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.pageslider').slick({
        'arrows': true,
        'dots': true,
        'slidesToShow': 2,
        'slidesToScroll': 1,
        'prevArrow': '<button type="button" class="slick-prev"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>',
        'nextArrow': '<button type="button" class="slick-next"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>',
        'responsive': [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.videobtn').on("click", function(e) {
        var self = $(this);
        var wrapper = $(self.attr('href'));
        var videoid = wrapper.data("videoid");
        var iframe = '<iframe id="videoframe" src="https://www.youtube.com/embed/' + videoid + '?autoplay=1" width="100%" height="100%" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>';
        $(wrapper).append(iframe);
        $('body').toggleClass('fixed');
        e.preventDefault();
    });
    $('.closebtn').on("click", function(e) {
        var self = $(this);
        $('#videoframe').remove();
        $('body').toggleClass('fixed');
    });

});
