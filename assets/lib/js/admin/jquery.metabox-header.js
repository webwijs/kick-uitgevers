(function($, window, document, undefined) {
    
    var HeaderMetabox = function(element, args)
    {
        /**
         * A reference to this object if the context changes.
         *
         * @typedef {HeaderMetabox}
         * @private
         */
        var self = this;
        
        /**
         * A jQuery element for which this plugin was created.
         *
         * @typedef {jQuery}
         */
        var $element = $(element);
        
        /**
         * A plain object containing key-value pairs.
         *
         * @type {Object}
         * @private
         */
        var options = {
            classes: {
                active: 'active',
            }
        };
        
        /**
         * Returns the element for the jQuery plugin was instantiated.
         *
         * @return {jQuery} a jQuery element.
         */
        self.getElement = function()
        {
            return $element;
        }
        
        /**
         * Returns the options set for the jQuery plugin.
         *
         * @reurn {Object} jQuery plugin options.
         */
        self.getOptions = function()
        {
            return options;
        }
        
        function init(args) {
            // merge plain object into options.
            options = $.extend({}, options, args);
        }
        init(args);
    }

    /**
     * Load an image asynchronously into the given container.
     *
     * @param {object} data a key-value object containing arguments.
     * @param {jQuery} a jQuery object which represents the element in which to load the image.
     * @throws {Error} if the second argument is not a jQuery object.
     */
    HeaderMetabox.prototype.loadImage = function(data, $container)
    {
        if (!($container instanceof jQuery)) {
            throw Error('loadImage(): expects a jQuery object as second argument.');
        }
    
        var defaults = {
            action: 'get_attachment_url',
            attachment_id: 0
        };
        data = $.extend({}, defaults, data);
        
        $container.empty().addClass('loading');

        $.post(ajaxurl, data, function(src) {
            if (typeof src === 'string' && src.length) {
                $(document.createElement('img')).attr("src", src).appendTo($container);
            }
        }).done(function() {
            $container.removeClass('loading'); 
        });
    }

    /**
     * Searches for an element within the metabox that matches the given selector and will
     * make it visible. All other visible elements will be hidden after this call returns.  
     *
     * Do note that calling this method for a non-existing element is considered a no-op.
     *
     * @param {string} selector a string containing a selector expression to match an element against.
     */
    HeaderMetabox.prototype.show = function(selector)
    {
        var options = this.getOptions();

        this.hide(this.format('.{0}', options.classes.active));
        
        this.getElement().find(selector).show().addClass(options.classes.active);
    }
    
    /**
     * Searches for an element within the metabox that matches the given selector and will
     * hide it.
     *
     * Do note that calling this method for a non-existing element is considered a no-op. 
     *
     * @param {string} selector a string containing a selector expression to match an element against.
     */
    HeaderMetabox.prototype.hide = function(selector)
    {        
        var options   = this.getOptions();

        this.getElement().find(selector).hide().removeClass(options.classes.active);

    }
    
    /**
     * Returns a formatted string according to the given arguments.
     *
     * @param {string} str the string to format.
     * @param {...string} a variable number of arguments to use in the format.
     * @return string a formatted string.
     * @throws {Error} if the first argument is not a string.
     * @access public
     */
    HeaderMetabox.prototype.format = function(str) {
        if (typeof str !== 'string') {
            throw new Error('format: can only operate on strings');
        }
        
        // get arguments to use in format function.
        var args = Array.prototype.slice.call(arguments);
        // remove first argument.
        args.shift();

        return str.replace(/{(\d+)}/g, function(match, number) { 
            return (typeof args[number] !== 'undefined') ? args[number] : match;
        });
    };
    
    /**
     * Create jQuery plugin.
     *
     * @param {Object} args a key-value pairs of options.
     * @see {@link http://learn.jquery.com/plugins/basic-plugin-creation/ Create a Basic Plugin}
     */
    $.fn.headerMetabox = function (args) {
        return this.each(function() {
            if (!$(this).data('header-metabox')) {
	            $(this).data('header-metabox', new HeaderMetabox(this, args));
			}
        });
    };
    
})(jQuery, this, this.document);
