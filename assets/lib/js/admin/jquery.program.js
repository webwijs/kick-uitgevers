jQuery(document).ready(function($) {
    var $container = $('.sortable-program-container.program');
    
    $.each($container, function(key, element) {
        var $list   = $(this).find('ol');
        var $button = $(this).find('.button');
        var $program = $(this).find('.program');
        var $day = $(this).find('.day');

        

        $list.sortable({
            placeholder: 'sortable-placeholder'
        });
        
        $(this).find('.button').click(function() {
            $stuff = { day: $day.val(), program: $program.val() };
            console.log($button.data('name'));

            $button = $(this);
            var data = {
                action: 'load_items',
                type: $button.data('name'),
                content: encodeURIComponent(JSON.stringify($stuff)),
                // content: JSON.stringify($stuff)
            }
            
            $.post(ajaxurl, data, function(response) {
                // console.log(response);     
                $list.append(response);
            });
        });
    });
    
    /**
     * Removes the tab by removing the list item which contains the tab.
     */
    $container.on('click', '.item-delete', function(event) {
        $(this).closest('li').remove();
        
        // prevent default action.
        event.preventDefault();
    }); 
});