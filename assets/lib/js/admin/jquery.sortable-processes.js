jQuery(document).ready(function($) {
    var $container = $('.sortable-posts-container');
    
    $.each($container, function(key, element) {
        var $list   = $(this).find('ol');
        var $button = $(this).find('.button');
        var $select = $(this).find('select');

        $list.sortable({
            placeholder: 'sortable-placeholder'
        });
        
        $(this).find('.button').click(function() {
            $button = $(this);
            $('option:selected', $select).each(function () {
                var data = {
                    action: 'load_sortable_item',
                    name: $button.data('name'),
                    post_id: $(this).val(),
                }
                
                $.post(ajaxurl, data, function(response) {                  
                    $list.append(response);
                });
            });
        });
    });
    
    /**
     * Removes the tab by removing the list item which contains the tab.
     */
    $container.on('click', '.item-delete', function(event) {
        $(this).closest('li').remove();
        
        // prevent default action.
        event.preventDefault();
    });	
});
