<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
    <title><?php wp_title('') ?></title>
    <meta charset="<?php bloginfo('charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/fav/favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/fav/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/fav/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/fav/manifest.json">
    <link rel="mask-icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/fav/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <?php
        $this->enqStyles(array(
        	array('name' => 'slick', 'src' => '/assets/lib/slick/slick.css'),
            array('name' => 'foundation', 'src' => '/assets/lib/foundation-flex/css/foundation.min.css'),
            array('name' => 'fancybox', 'src' => '/assets/lib/fancybox/dist/jquery.fancybox.min.css'),
        	array('name' => 'main', 'src' => '/assets/css/main.css', 'deps' => array('foundation')),
        ));
    ?>

    <?php wp_enqueue_script('jquery') ?>
    <?php wp_enqueue_script('foundation', get_bloginfo('stylesheet_directory') . '/assets/lib/foundation-flex/js/vendor/foundation.min.js', array('jquery'),'',true) ?>
    <?php wp_enqueue_script('fancybox', get_bloginfo('stylesheet_directory') . '/assets/lib/fancybox/dist/jquery.fancybox.min.js', array('jquery'),'',true) ?>
    <?php wp_enqueue_script('modernizr', get_bloginfo('stylesheet_directory') . '/assets/lib/js/modernizr.js', array('jquery'),'',true) ?>
    <?php wp_enqueue_script('slick', get_bloginfo('stylesheet_directory') . '/assets/lib/slick/slick.min.js', array('jquery'),'',true) ?>
    <?php //wp_enqueue_script('masonry', get_bloginfo('stylesheet_directory') . '/assets/js/masonry-ordered.js', array('jquery'),'',true) ?>
    <?php wp_enqueue_script('google', "//maps.google.com/maps/api/js?key=AIzaSyA819HrIwc3cjPntSqkUEYsAPzLRahLytA", array('jquery'), false, true) ?>
    <?php wp_enqueue_script('main', get_bloginfo('stylesheet_directory') . '/assets/js/main.js', array('jquery'),'',true) ?>
    <?php wp_head() ?>
    <script>
      (function(d) {
        var config = {
          kitId: 'wvk2oiw',
          scriptTimeout: 3000,
          async: true
        },
        h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
      })(document);
    </script>
</head>

<body <?php body_class() ?> itemscope itemtype="http://schema.org/WebPage">
    
    <?php echo $this->partial('partials/layout/header.phtml') ?>

    <section class="main-container" role="main">
        <?php echo $this->content ?>
    </section>
    
    <?php echo $this->partial('partials/layout/footer.phtml'); ?>

    <?php if($videoid = get_option('theme_video_id')): ?>
        <div id="videopopup" data-videoid="<?php echo $videoid; ?>">
            <a class="closebtn toggle" href="#videopopup">sluiten</a>
        </div>
    <?php endif; ?>

    <?php wp_footer() ?>
</body>
</html>
