<?php

namespace Webwijs\Dom;

/**
 * The HtmlElementBuilder is a concrete implementation of the {@link HtmlElementBuilderInterface}.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class HtmlElementBuilder
{
    /**
     * A collection of HTML attributes.
     *
     * @array
     */
    private $attributes = array();

    /**
     * A collection of arbitrary data.
     *
     * @var array
     */
    private $data = array();
    
    /**
     * A collection of {@link ElementInterface} instances.
     *
     * @var array
     */
    private $children = array();
  
    /**
     * The HTML tag.
     *
     * @var string
     */
    private $tag = 'div';
  
    /**
     * The inner text.
     *
     * @var string
     */
    private $text = '';

    /**
     * Construct a new HtmlElementBuilder.
     *
     * @param string $tag (optional) the HTML tag.
     */
    public function __construct($tag = 'div')
    {
        $this->tag($tag);
    }

    /**
     * Set a collection of attributes for the element.
     *
     * @param array|Traversable $attributes a collection of attributes for the element.
     * @return HtmlElementBuilderInterface the builder which allows further creation of the element.
     * @throws InvalidArgumentException if the specified argument is not an array or Traversable object.
     * @see HtmlElementBuilder::attribute($name, $value)
     */
    public function attributes($attributes)
    {
        if (!is_array($attributes) && !$attributes instanceof \Traversable) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects an array or instance of the Traversable; received "%s"',
                __METHOD__,
                (is_object($attributes) ? get_class($attributes) : gettype($attributes))
            ));
        }
        
        foreach ($attributes as $name => $value) {
            $this->attribute($name, $value);
        }
        return $this;
    }
    
    /**
     * {@inheritDoc}
     *
     * @throws InvalidArgumentException if the specified argument is not an array or Traversable object.
     */
    public function attribute($name, $value)
    {
        if (!is_string($name)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects the first argument to be a string; received "%s" instead.',
                __METHOD__,
                (is_object($name)) ? get_class($name) : gettype($name)
            ));
        }
    
        $this->attributes[$name] = $value;
        return $this;
    }
    
    /**
     * {@inheritDoc}
     *
     * @throws InvalidArgumentException if the first argument is not a string.
     */
    public function data($name, $value)
    {
        if (!is_string($name)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects the first argument to be a string; received "%s" instead.',
                __METHOD__,
                (is_object($name)) ? get_class($name) : gettype($name)
            ));
        }
            
        $this->data[$name] = $value;
        return $this;
    }
   
    /**
     * {@inheritDoc}
     *
     * @throws InvalidArgumentException if the specified argument is not a string.
     */
    public function tag($tag)
    {
        if (!is_string($tag)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects the specified argument to be a string; received "%s" instead.',
                __METHOD__,
                (is_object($tag)) ? get_class($tag) : gettype($tag)
            ));
        }
        
        $this->tag = $tag;
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function child($child)
    {
        if (is_callable($child)) {
            $child = call_user_func($child, new self());
        }
        
        if (!$child instanceof ElementInterface) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects child to be an ElementInterface instance; received "%s" instead.',
                __METHOD__,
                (is_object($child)) ? get_class($child) : gettype($child)
            ));
        }
        
        $this->children[] = $child;
        return $this;
    }
    
    /**
     * {@inheritDoc}
     *
     * @throws InvalidArgumentException if the specified argument is not a string.
     */ 
    public function text($text)
    {
        if (!is_string($text)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects the specified argument to be a string; received "%s" instead.',
                __METHOD__,
                (is_object($text)) ? get_class($text) : gettype($text)
            ));
        }
        
        $this->text = $text;
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function build()
    {
        $element = new HtmlElement($this->tag);
        $element->addAttributes($this->attributes);
        $element->addChildren($this->children);
        $element->setInnerText($this->text);

        foreach ($this->data as $name => $value) {
            $element->addData($name, $value);
        }
        
        return $element;
    }
}
