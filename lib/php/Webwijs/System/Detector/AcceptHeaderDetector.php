<?php

namespace Webwijs\System\Detector;

use Webwijs\System\Device;

use Webwijs\Http\Request;

/**
 * A detector that determines the device based on the browser's accept header.
 *
 * @author Chris Harris
 * @version 0.0.9
 */
class AcceptHeaderDetector extends AbstractDetector
{
    /**
     * A collection of phone headers.
     *
     * @var array
     */
    private $phone = array(
        'text\/vnd.wap.wml',
        'application\/vnd.wap.xhtml+xml'
    );

    /**
     * {@inheritDoc}
     */
    public function detect(Request $request)
    {
        $detected = -1;
        if (preg_match($this->getPhoneRegex(), $request->getServer('HTTP_ACCEPT', ''))) {
            $detected = Device::PHONE;
        } else if ($this->detector !== null) {
            $detected = $this->detector->detect($request);
        }

        return $detected;
    }

    /**
     * Returns a regular expression to detect the accept header of a phone.
     *
     * @return string a regular expression.
     */
    private function getPhoneRegex()
    {
        return sprintf('#%1$s#i', implode('|', $this->phone));
    }
}
