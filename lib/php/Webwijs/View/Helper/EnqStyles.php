<?php
namespace Webwijs\View\Helper;

use \DateTime;
use \Exception;

use Webwijs\Util\Arrays;
use Webwijs\Util\Strings;

/**
 * A helper to enqueue multiple stylesheets and create a version number according to the modification time of the stylesheet.
 *
 * @author Joris Wagter
 */
class EnqStyles
{
	public function enqStyles(array $sheets = array())
	{
		$i = 0;

		foreach ($sheets as $s) {

			$i++;

			$defaults = array(
				'prefixWeb' => get_bloginfo('stylesheet_directory'),
				'prefixReal' => get_stylesheet_directory(),
				'name' => 'css_' . $i,
				'src' => '',
				'deps' => array(),
				'ver' => '',
				'media' => 'all',
			);

			$sheet = Arrays::addAll($defaults, $s);

			if (is_string($sheet['src']) && $sheet['src'] !== '') {
				$serverPath = $sheet['prefixReal'] . $sheet['src'];
				if (is_file($serverPath)){
					if (Strings::endsWith($serverPath, '.css')) {
						// get file's webpath
						$webPath = $sheet['prefixWeb'] . $sheet['src'];
						// get file's modification time
						if ($sheet['ver'] === '') {
							$mTime = new DateTime();
							$mTime->setTimestamp(filemtime($serverPath));
							$sheet['ver'] = $mTime->format('Y-m-d_H-i-s');
						}

						// if sheet['deps'] is string, convert it to an array
						if (is_string($sheet['deps'])) {
							$deps = Strings::toArray($sheet['deps']);
						} else {
							$deps = $sheet['deps'];
						}

						// enqueue stylesheet
						wp_enqueue_style($sheet['name'], $webPath, $deps, $sheet['ver'], $sheet['media']);
						
					} else {
						throw new Exception(sprintf('Make sure the src index of entry with name: %s points to a CSS file', $sheet['name']));
					}
				} else {
					throw new Exception(sprintf('File with path: %s does not exist', $serverPath));
				}
			} else {
				throw new Exception(sprintf('Please enter a string with relative source path for index src of entry with name %s', $sheet['name']));
			}
		}
	}
}
