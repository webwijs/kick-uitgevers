<?php

namespace Webwijs\View\Helper;

class FormNumber extends FormElement
{
    public function formNumber($name, $value, $attribs = array(), $options = array())
    {
        $attribs['type'] = 'number';
        $attribs['value'] = ($this->escape($value))? $this->escape($value) : 1;
        $attribs['name'] = $name;
        !isset($attribs['id']) && $attribs['id'] = $name . '-input';
        return '<input' . $this->_renderAttribs($attribs) . '/>';
    }
}
