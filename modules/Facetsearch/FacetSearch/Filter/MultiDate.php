<?php

namespace Webwijs\FacetSearch\Filter;

use DateTime;

use Webwijs\FacetSearch\Filter\AbstractFilter;

class MultiDate extends AbstractFilter
{
    /**
     * The expeceted date format.
     *
     * @var string
     */
    const DATE_FORMAT = 'd-m-Y';

    /**
     * A collection of filter options.
     *
     * @var array
     */
    public $filterOptions = array(
        'meta_end' => '_multidate_until_timestamp',
        'meta_start' => '_multidate_from_timestamp'
    );

    /**
     * Returns a subcollection of arguments which are supplied to a {@link WP_Query} instance.
     *
     * @return array arguments used by a WP_Query object.
     */
    public function apply()
    {
        $filterOpts = $this->filterOptions;
        $filterOpts = array(
            'meta_end' => '_multidate_until_timestamp',
            'meta_start' => '_multidate_from_timestamp',
        );
        $timestamps = $this->getTimestamps();

        $metaQuery = array('meta_query' => array(
            'relation' => 'OR',
            array(
                'key'     => $filterOpts['meta_end'],
                'value'   => array($timestamps[0], $timestamps[1]),
                'type'    => 'numeric',
                'compare' => 'BETWEEN',
            ),
            array(
                'key'     => $filterOpts['meta_start'],
                'value'   => array($timestamps[0], $timestamps[1]),
                'type'    => 'numeric',
                'compare' => 'BETWEEN',
            ),
            array(
                'relation' => 'AND',
                array(
                    'key'     => $filterOpts['meta_start'],
                    'value'   => $timestamps[0],
                    'type'    => 'numeric',
                    'compare' => '<=',
                ),
                array(
                    'key'     => $filterOpts['meta_end'],
                    'value'   => $timestamps[1],
                    'type'    => 'numeric',
                    'compare' => '>=',
                )
            )
        ));
        
        return $metaQuery;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setValue($value)
    {    
        // ensure the array contains two dates.
        $this->value = (is_array($value)) ? $value : array($value);
        if (($size = count($this->value)) !== 2) {
            $this->value = ($size > 2) ? array_slice($this->value, 0, 2) : array_pad($this->value, 2, null);
        }
                
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getValue()
    {      
        // ensure the array contains two dates.  
        $dates = (is_array($this->value)) ? $this->value : array();
        if (($size = count($dates)) !== 2) {
            $dates = ($size > 2) ? array_slice($dates, 0, 2) : array_pad($dates, 2, null);
        }
        
        return array_values($dates);
    }
    
    /**
     * Returns a timestamp for each date contained by this filter.
     *
     * @return array a collection of timestamps.
     */
    public function getTimestamps()
    {
        $dates  = $this->getValue();
        $format = $this->getFormat();
        
        if (!empty($dates[0])) {
            $from = DateTime::createFromFormat($format, $dates[0]);
        } else {
            $from = new DateTime();
            $from->setTimestamp(current_time('timestamp'));
        }
        
        if (!empty($dates[1])) {
            $until = DateTime::createFromFormat($format, $dates[1]);
        } else {
            $until = clone $from;
            $until->modify('+30 days');
        }

        return array($from->getTimestamp(), $until->getTimestamp());
    }
    
    /**
     * Returns the expected format of a date string.
     *
     * @return string the format of a date string.
     * @link http://php.net/manual/en/function.date.php date format
     */
    public function getFormat()
    {
        return self::DATE_FORMAT;
    }
}
