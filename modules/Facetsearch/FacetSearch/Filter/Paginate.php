<?php

namespace Module\Facetsearch\FacetSearch\Filter;

use Webwijs\FacetSearch\Filter\AbstractFilter;
use Webwijs\FacetSearch\Filter\PaginateInterface;

class Paginate extends AbstractFilter implements PaginateInterface
{
    public $currentPage = 1;
    public function apply()
    {
        $args = array();
        $pages = $this->getValue();
        $postsPerPage = $this->getCaller()->defaults['posts_per_page'];
        
        if (preg_match('/^(?:(\d+)-)?(\d+)$/', $pages, $match)) {
            $start = (int) $match[1];
            $end = (int) $match[2];
            if (!empty($start) && ($start < $end)) {
                $args['offset'] = $postsPerPage * ($start - 1);
                $args['posts_per_page'] = $postsPerPage * ($end - ($start - 1));
            }
            else {
                $args['paged'] = $end;
            }
            $this->currentPage = $end;
        }
        return $args;        
    }
    public function getNextLink()
    {
        $page = (int) $this->getValue();
        return $this->getPageLink($page + 1);
    }
    public function getPageLink($page)
    {
        $params = $this->getCaller()->getParams();
        $params[$this->name] = $page;
        return get_permalink() . '?' . urlencode($this->getCaller()->buildQuery($params));
    }
    public function getNextPage()
    {
        $page = (int) $this->getValue();
        return $page + 1;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }
    
    public function getNumPages()
    {
        return ceil($this->getCaller()->getWpQuery()->found_posts / $this->getCaller()->defaults['posts_per_page']);
    }

    /**
     * {@inheritDoc}
     *
     * @throws InvalidArgumentException if the specified argument is not a number.
     */
    public function getPaginationUrl($paged)
    {
        if (!is_numeric($paged)) {
            Throw new \InvalidArgumentException(sprintf(

            ));
        }

        $params = $this->getCaller()->getParams();
        $params[$this->name] = (int) $paged;
        return get_permalink() . '?' . urlencode($this->getCaller()->buildQuery($params));
    }
}
