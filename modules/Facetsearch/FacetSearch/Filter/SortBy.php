<?php

namespace Webwijs\FacetSearch\Filter;

class SortBy extends AbstractFilter implements MultiFilterInterface
{
    public function apply()
    {
        // var_dump($this->value);
        // exit;
        if (!empty($this->value)) {
            $args = array();
            $args['orderby'] = $this->value;
            return $args;
        }
    }
    /**
     * {@inheritDoc}
     */
    public function getOptions()
    {
        $options = array();
        $sortoptions = array(
            'post_title' => 'Naam',
            'post_date' => 'Datum'
        );
        return $sortoptions;
    }
    

}
