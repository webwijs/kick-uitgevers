<?php

namespace Webwijs\FacetSearch\Filter;

class SortOrder extends AbstractFilter implements MultiFilterInterface
{
    public function apply()
    {
        if (!empty($this->value)) {
            $args = array();
            $args['order'] = $this->value;
            return $args;
        }
    }
    /**
     * {@inheritDoc}
     */
    public function getOptions()
    {
        $options = array();
        $sortoptions = array(
            'asc' => 'Laag - hoog',
            'desc' => 'Hoog - laag'
        );
        return $sortoptions;
    }


}
