<?php

namespace Module\Facetsearch\FacetSearch;

use Webwijs\FacetSearch\AbstractFacetSearch as AbstractFacetedSearch;

class News extends AbstractFacetedSearch
{
    public function init()
    {
        $this->defaults = array(
            'orderby' => 'post_date',
            'order' => 'desc',
            'posts_per_page' => 6,
            'post_type' => 'news'
        );
        $this->addFilter('taxonomy', 'post', array('taxonomy'=>'news_category'));
        $this->addFilter('paginate', 'paginate', array('default' => 1));
    }
}

