<?php

namespace Module\Facetsearch\FacetSearch;

use Webwijs\FacetSearch\AbstractFacetSearch as AbstractFacetedSearch;

class Posts extends AbstractFacetedSearch
{
    public function init()
    {
        $this->defaults = array(
            'orderby' => 'post_date',
            'order' => 'desc',
            'posts_per_page' => 6,
            'post_type' => 'post'
        );
        $this->addFilter('taxonomy', 'post', array('taxonomy'=>'category'));
        $this->addFilter('paginate', 'paginate', array('default' => 1));
    }
}

