<?php

namespace Module\Facetsearch\Helper;

use Webwijs\FacetSearch\Filter\MultiFilterInterface;
use Webwijs\Util\Strings;
use Webwijs\View\Helper\FormElement;

/**
 * The FacetedMulticheckox should be considered as an adapter that allows the 'formMulticheckbox' to work
 * with classes that implement the MultiFilterInterface.
 *
 * @author Chris Harris
 * @version 0.0.1
 */
class facetedMulticheckox extends FormElement
{
    /**
     * Displays one or more checkbox for the given filter.
     *
     * @param MultiFilterInterface $filter the filter for which to display checkboxes.
     * @param array|null $attribs (optional) array of attributes to set for each checkbox.
     */
    public function facetedMulticheckox(MultiFilterInterface $filter, $attribs = array())
    {
        return $this->createCheckboxes($filter->getName(), $filter->getValue(), $attribs, $filter->getOptions());
    }
    
    /**
     * @param string $name the name for this multicheckbox.
     * @param mixed $value the value(s) for selected checkboxes.
     * @param array attributes that will be added to each checkbox.
     * @param array|string $options options for which checkboxes will be created.
     */
    private function createCheckboxes($name, $value, $attribs, $options)
    {
        if (is_string($options)) {
            $options = Strings::toArray($options);
        }
    
        $output = '';
        foreach ($options as $optionValue => $optionLabel) {
            
            $optionLabel = (is_array($optionLabel)) ? $optionLabel['label'] : $optionLabel;

            $optionLabel = (($optionLabel == 'Actie') || ($optionLabel == 'Accomodatie')) ? 'Toon alleen de '.strtolower($optionLabel).'s' : $optionLabel;
        
            $labelAttribs = array();
        
            $optionAttribs = array();
            $optionAttribs['name'] = $name . '[]';
            $optionAttribs['value'] = $optionValue;
            $optionAttribs['type'] = 'checkbox';

            if (isset($options[$optionValue]['count']) && $options[$optionValue]['count'] == 0) {
                $labelAttribs['class'] = 'disabled';
                $optionAttribs['disabled'] = 'disabled';
                continue;
            }

            if((is_array($value) && in_array($optionValue, $value)) || $optionValue == $value){
                $optionAttribs['checked'] = 'checked';
            }
            $output .= '<div class="boxholder">';
            $output .= '<div class="checkbox">';
            $output .= '<input id="filter_'.$optionValue.'"'. $this->_renderAttribs($optionAttribs) . ' />';
            $output .= '<label for="filter_'.$optionValue.'" '. $this->_renderAttribs($labelAttribs) .'></label>';
            $output .= '</div>';
            $output .= '<label class="boxlabel" for="filter_'.$optionValue.'" '. $this->_renderAttribs($labelAttribs) .'>'. $optionLabel . '</label>';
            $output .= '</div>';
        }
        
        return $output;
    }
}

