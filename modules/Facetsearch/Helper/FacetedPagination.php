<?php

namespace Module\FacetSearch\Helper;

use Webwijs\FacetSearch\Filter\PaginateInterface;

class FacetedPagination
{   
    /**
	 * The paginate filter.
	 * 
	 * @var PaginateInterface
	 */
	private $filter = null;

	private $args = array(
        'title' => '',
        'nextpage' => '&raquo;',
        'previouspage' => '&laquo;',
        'before' => '<nav>',
        'after' => '</nav>',
        'empty' => true,
        'range' => 3,
        'anchor' => 1,
        'gap' => 3
    );

    public function facetedPagination(PaginateInterface $filter, array $args = array())
    {
    	$this->args = array_merge($this->args, $args);
    	$this->filter = $filter;

        $pages = $filter->getNumPages();
        $page  = $filter->getCurrentPage();

 		$prevUrl = $filter->getPaginationUrl($page - 1);
 		$nextUrl = $filter->getPaginationUrl($page + 1);

        $output = stripslashes($this->args['before']);
        if ($pages > 1) {
            $output .= '<ol class="wp-paginate faceted-paginate" data-filter-name="paginate">';

            $ellipsis = '<li><span class="gap">...</span></li>';

            if ($page > 1 && !empty($this->args['previouspage'])) {
                $output .= sprintf('<li><a href="%s" class="prev" data-page="%s">%s</a></li>', $prevUrl, ($page - 1), stripslashes($this->args['previouspage']));
            }

            $minLinks  = $this->args['range'] * 2 + 1;
            $blockMin  = min($page - $this->args['range'], $pages - $minLinks);
            $blockHigh = max($page + $this->args['range'], $minLinks);
            $leftGap   = (($blockMin - $this->args['anchor'] - $this->args['gap']) > 0) ? true : false;
            $rightGap  = (($blockHigh + $this->args['anchor'] + $this->args['gap']) < $pages) ? true : false;

            if ($leftGap && !$rightGap) {
                $output .= sprintf('%s%s%s',
                    $this->paginateLoop(1, $anchor),
                    $ellipsis,
                    $this->paginateLoop($blockMin, $pages, $page)
                );
            } else if ($leftGap && $rightGap) {
                $output .= sprintf('%s%s%s%s%s',
                    $this->paginateLoop(1, $anchor),
                    $ellipsis,
                    $this->paginateLoop($blockMin, $blockHigh, $page),
                    $ellipsis,
                    $this->paginateLoop(($pages - $this->args['anchor'] + 1), $pages)
                );
            } else if ($rightGap && !$leftGap) {
                $output .= sprintf('%s%s%s',
                    $this->paginateLoop(1, $blockHigh, $page),
                    $ellipsis,
                    $this->paginateLoop(($pages - $this->args['anchor'] + 1), $pages)
                );
            } else {
                $output .= $this->paginateLoop(1, $pages, $page);
            }

            if ($page < $pages && !empty($this->args['nextpage'])) {
                $output .= sprintf('<li><a href="%s" class="next" data-page="%s">%s</a></li>', $nextUrl, ($page + 1), stripslashes($this->args['nextpage']));
            }
            $output .= "</ol>";
        }
        $output .= stripslashes($this->args['after']);

        if ($pages > 1 || $this->args['empty']) {
            return $output;
        }
    }

    private function paginateLoop($start, $max, $page = 0) {
        $output = "";
        for ($i = $start; $i <= $max; $i++) {
            $p = $this->filter->getPaginationUrl($i);
            $output .= ($page == intval($i))
                ? "<li class=\"page current\"><span>{$i}</span></li>"
                : "<li class='page'><a href=\"{$p}\" title=\"{$i}\" data-page=\"{$i}\">{$i}</a></li>";
        }
        return $output;
    }
}

