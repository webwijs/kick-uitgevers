<?php

namespace Module\Facetsearch\Helper;

use Webwijs\FacetSearch\Filter\MultiFilterInterface;
use Webwijs\Util\Strings;
use Webwijs\View\Helper\FormElement;

/**
 * The FacetedSelectbox should be considered as an adapter that allows the 'FacetedSelectbox' to work
 * with classes that implement the MultiFilterInterface.
 *
 * @author Mark Postema
 * @version 0.0.1
 */
class FacetedSelectbox extends FormElement
{
    /**
     * Displays one or more checkbox for the given filter.
     *
     * @param MultiFilterInterface $filter the filter for which to display a selectbox.
     * @param array|null $attribs (optional) array of attributes to set for each option.
     */
    public function facetedSelectbox(MultiFilterInterface $filter, $attribs = array())
    {
        return $this->createSelectbox($filter->getName(), $filter->getValue(), $attribs, $filter->getOptions(), $filter->filterOptions['first']);
    }
    
    /**
     * @param string $name the name for this selectbox.
     * @param mixed $value the value(s) for selected options.
     * @param array attributes that will be added to each option.
     * @param array|string $options options for which options will be created.
     */
    private function createSelectbox($name, $value, $attribs, $options, $first = null)
    {
        if (is_string($options)) {
            $options = Strings::toArray($options);
        }

        $output = '<select id="' . $name . '-input" name="'.$name.'">';

        if($first){
            $output .= '<option value="">'.$first.'</option>';
        }

        foreach ($options as $optionValue => $optionLabel) {

            $optionLabel = (is_array($optionLabel)) ? $optionLabel['label'] : $optionLabel;
        
            $disabled = '';
            if (isset($options[$optionValue]['count']) && $options[$optionValue]['count'] == 0) {
                $disabled = 'disabled';
            }

            $selected = '';
            if((is_array($value) && in_array($optionValue, $value)) || $optionValue == $value){
                $selected = 'selected';
            }
            $output .= '<option alt="'.$value.'" value="'.$optionValue.'" '.$selected.' '.$disabled.'>'.$optionLabel.'</option>';
        }
        $output .= '</select>';
        return $output;
    }
}
