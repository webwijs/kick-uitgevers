(function($, window, document, undefined) {
    "use strict";
    
    /**
     * The FacetedSearch is targeted at accesing information according to a faceted classification system. 
     *
     * This faceted search operates on a HTML form whose elements acts as filters. The state of each filter has an 
     * effect on the information that is displayed by the faceted search. Fore more detailed information on faceted 
     * search see {@link http://en.wikipedia.org/wiki/Faceted_search Faceted search}.
     *
     * @author Chris Harris
     * @version 0.0.2
     */
    function FacetedSearch(element, args) {
        /**
         * A reference to this object if the context changes.
         *
         * @typedef {MultiSlider}
         * @private
         */
        var self = this;
        
        /**
         * A jQuery element for which this plugin was created.
         *
         * @typedef {jQuery}
         */
        var $container = $(element);
        
        /**
         * A plain object containing key-value pairs.
         *
         * @type {Object}
         * @private
         */
        var options = {
            url: '',
            target: '.faceted-search-target',
            form: '.filter-form',
            button: '.readmore',
            autoSubmit: true
        }
        
        /**
         * A (unique) string for the current update.
         *
         * @var string
         */
        var currentUpdateId = '';
        
        /**
         * A string describing the action.
         *
         * @var string
         */
        var action = '';
        
        /**
         * Returns the options used by this plugin.
         *
         * @return {Object}
         * @public
         */
        self.getOptions = function() {
            return options;
        }
        
        /**
         * Returns a jQuery element for which this plugin was created.
         *
         * @return {jQuery}
         */
        self.getContainer = function() {
            return $container;
        }
        
        function update(event) { 
            
            if (event.value == currentUpdateId) {
                return false;
            }
            
            // add loading class.
            setLoading(true);
            // update current id.    
            currentUpdateId = event.value;

            var filters = currentUpdateId.split('&');
            for (var i = 0; i < filters.length; i++) {
                var temp = decodeURI(filters[i]);
                var parts = temp.split('[]=');
                $('#filter_'+parts[1]).prop('checked', true);
            }
            if(parts[0] == 'clear'){
                $('#post-input input').each(function(){
                    $(this).prop('checked', false);
                });
            }
            
            
            if (typeof options.url === 'string' && options.url.length > 0) {
                // test whether url contains a query string.
                var hasQueryString = function (url) {
                    var regex = /[?&](.*)=(.*)$/;                    
                    return regex.test(url);
                }
                
                // determine whether an ampersand or question mark is required.
                if (!hasQueryString(options.url)) {
                    var url = self.format('{0}?{1}', options.url, event.value);

                } else {
                    var url = self.format('{0}&{1}', options.url, event.value);
                }

                console.log(url);
                
                $.getJSON(url, function(data) {          
                    // append items to list.

                    // console.log(data);

                    if (data.hasOwnProperty('items')) {
                        var $target = self.target().empty();
                        $.each(data.items, function (index, item) {
                            $target.append($(item));
                        });
                    }
                    
                    // update current page.
                    if (data.hasOwnProperty('page') && data.hasOwnProperty('pages') && data.page < data.pages) {
                        self.button().attr('data-current-page', data.page);
                    }

                    // remove loading class.
                    setLoading(false);
                    // trigger updated listener.
                    $container.trigger('updated', data);
                });
            }
        }
        
        /**
         * Add a 'loading' class to the faceted search.
         *
         * @param {boolean} if true will add a loading class, otherwise the clas is removed.
         * @private
         */
        function setLoading(loading) {
            loading = (typeof loading === 'boolean') ? loading : false;
            
            var className = 'loading';
            if (loading) {
                if (action == 'paginate') {
                    self.button().addClass(className);
                } else {
                    self.getContainer().addClass(className);
                }
            } else {
                if (action == 'paginate') {
                    self.button().removeClass(className);
                } else {
                    self.getContainer().removeClass(className);
                }
            }
        }
        
        /**
         * Returns the serialized data from the given form.
         *
         * In order to successfully serialize the given form all 'disabled' attributes are temporarily 
         * removed. After serializing the all elements that were disabled will be disabled again.
         *
         * @param {jQuery} $form the form to serialize.
         * @return {string} serialized data contained by the form.
         */
        function serialize($form) {
            $('label.disabled input[type=checkbox]', $form).removeAttr('disabled');
            var serialized = $form.serialize();
            $('label.disabled input[type=checkbox]', $form).attr('disabled', 'disabled');

            return serialized;
        }
        
        /**
         * Applies a key-value pair for pagination to the end of the serialized form.
         *
         * The pagination button should have a data attribute called 'filter-name' that contains
         * the name of the filter and and data attribute called 'current-page' that indicates which
         * page has been loaded thus far.
         *
         * @param {jQuery{ $button the pagination button.
         * @return {string} serialized data from the form and the pagination appended to it.
         */
        function paginate($button) {
            var serialized = serialize(self.form());

            var name = $button.data('filter-name');

            if (typeof name === 'string') {
                var current = $button.attr('data-current-page');
                var next = ($.isNumeric(current)) ? (parseInt(current) + 1) : 1;

                if (typeof serialized === 'string' && serialized.length > 0) {
                    serialized = self.format('{0}&{1}=1-{2}', serialized, name, next);
                } else {
                    serialized = self.format('{0}=1-{1}', name, next);
                }
            }

            return serialized;
        }
        
        /**
         * A handler that is called whenever the form has changed.
         *
         * @param {MouseEvent} event object describing the event that has occurred.
         */
        function onFormChange(event) {
            $(this).submit();
        }
        
        /**
         * A handler that is called whenever an asynchronous call has returned.
         *
         * @param {Event} event object describing the event that has occurred.
         * @param {Object} a plain object that contains information from the asynchronous call.
         * @see update(event)
         */
        function onUpdate(event, data) {               
            // display or hide the paginate button.
            if (data.hasOwnProperty('page') && data.hasOwnProperty('pages') && (data.page >= data.pages)) {
                self.button().addClass('disabled').hide();
            } else {
                self.button().removeClass('disabled').fadeIn();
            }
        }
        
        /**
         * Initialize object.
         *
         * @private
         */
        function init(args) {
            // merge plain objects into options.
            options = $.extend({}, options, args);
            $container.on('updated', onUpdate);

            if (self.autoSubmit()) {
                self.form().on('change', onFormChange);
            }
            
            $.address.tracker(null).strict(false).init(function(event) {
                self.form().address(function() {
                    action = 'serialize';
                    return serialize($(this));
                });
                
                self.button().address(function() {
                    action = 'paginate';
                    return paginate($(this));
                });
            }).change(function(event) {
                update(event);
            });
        }
        init(args);
    }
    
    /**
     * Returns true if the underlying form will auto submit, false otherwise.
     *
     * @return {boolean} true if the form will auto submit, false otherwise.
     */
    FacetedSearch.prototype.autoSubmit = function() {
        var autoSubmit = false;
        if (typeof this.getOptions().autoSubmit === 'boolean') {
            autoSubmit = this.getOptions().autoSubmit;
        }
        
        return autoSubmit;
    }
    
    /**
     * Returns a jQuery object form a form element.
     *
     * @return {jQuery} jQuery object form a form element.
     */
    FacetedSearch.prototype.form = function() {
        var $form = this.getOptions().form;
        if (!($form instanceof jQuery)) {
            $form = $(this.getOptions().form, this.getContainer());
        }
        
        return $form;
    }
    
    /**
     * Returns a jQuery object for an HTML element which displays all results.
     *
     * @return {jQuery} jQuery object for an HTML element.
     */
    FacetedSearch.prototype.target = function() {
        var $target = this.getOptions().target;
        if (!($target instanceof jQuery)) {
            $target = $(this.getOptions().target, this.getContainer());
        }
        
        return $target;
    }
    
    /**
     * Returns a jQuery object for an HTML element that is used to paginate.
     *
     * @return {jQuery} jQuery object for an HTML element.
     */
    FacetedSearch.prototype.button = function() {
        var $button = this.getOptions().button;
        if (!($button instanceof jQuery)) {
            $button = $(this.getOptions().button, this.getContainer());
        }
        
        return $button;
    }
    
    /**
     * Returns a formatted string according to the given arguments.
     *
     * @param {string} str the string to format.
     * @param {...string} a variable number of arguments to use in the format.
     * @return string a formatted string.
     * @throws {Error} if the first argument is not a string.
     * @access public
     */
    FacetedSearch.prototype.format = function(str) {
        if (typeof str !== 'string') {
            throw new Error('format: can only operate on strings');
        }
        // get arguments to use in format function.
        var args = Array.prototype.slice.call(arguments);
        // remove first argument.
        args.shift();

        return str.replace(/{(\d+)}/g, function(match, number) { 
            return (typeof args[number] !== 'undefined') ? args[number] : match;
        });
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} args a key-value pairs of options.
     * @see {@link http://learn.jquery.com/plugins/basic-plugin-creation/ Create a Basic Plugin}
     */
    $.fn.facetedSearch = function (args) {
        return this.each(function() {
            if (!$(this).data('faceted-search')) {
		        $(this).data('faceted-search', new FacetedSearch(this, args));
			}
        });
    };
    
    // add FacetedSearch to the global scope.
    window.FacetedSearch = FacetedSearch;
    
})(jQuery, this, this.document);
