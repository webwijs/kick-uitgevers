<?php

namespace Module\Gallery\Action;

use Webwijs\View;

class Ajax
{
    public static function addGalleryImage()
    {
        $view = new View();
    
        $thumbnail_id = (isset($_POST['thumbnail_id'])) ? intval($_POST['thumbnail_id']) : null;
        $meta_key = (isset($_POST['relation_key'])) ? strip_tags($_POST['relation_key']) : null; 
        $counter = (isset($_POST['counter'])) ? intval($_POST['counter']) : 0;

        if(!empty($thumbnail_id)) {
            $post = get_post($thumbnail_id);
            if(is_object($post)) {
                echo $view->rowBuilder($post, array('meta_key' => $meta_key, 'counter' => $counter));
                exit;
            }
        }

        die(0);
    }
}
