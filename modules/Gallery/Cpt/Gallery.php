<?php

namespace Module\Gallery\Cpt;

use Webwijs\Cpt;

class Gallery extends Cpt
{
    public $type = 'gallery';
    
    public function init()
    {
        $this->labels = array(
            'name' => __('Gallerij', 'theme'),
            'singular_name' => __('Gallerij', 'theme'),
            'add_new' => __('Nieuwe gallerij', 'theme'),
            'add_new_item' => __('Nieuwe gallerij', 'theme'),
            'edit_item' => __('Gallerij bewerken', 'theme'),
            'new_item' => __('Nieuwe gallerij', 'theme'),
            'view_item' => __('Gallerij bekijken', 'theme'),
            'search_items' => __('Gallerij zoeken', 'theme'),
            'not_found' => __('Geen gallerijen gevonden', 'theme'),
            'not_found_in_trash' => __('Geen gallerijen gevonden', 'theme'),
            'menu_name' => __('Gallerijen', 'theme')
        );
        
        $this->settings = array(
            'rewrite' => false,
            'hierarchical' => false,
            'public' => false,
            'show_ui' => true,
            'supports' => array('title')
        );
    }
    
    public static function register($options = null)
    {
        $cpt = new self($options);
    }
    
    public static function queryPosts($args = null)
    {
        $defaults = array(
            'post_type' => 'gallery',
            'orderby' => 'menu_order',
            'order' => 'ASC',
        );
        $args = array_merge($defaults, (array) $args);
        return query_posts($args);   
    }
}
