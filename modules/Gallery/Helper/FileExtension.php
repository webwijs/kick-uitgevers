<?php

namespace Module\Gallery\Helper;

class FileExtension
{    
    /**
     * Returns the file extension for the given post.
     *
     * @param object the post object from which the extension will be returned.
     * @return string returns the file extension for the given post.
     */
    public function fileExtension($post)
    {
        if(empty($post)) {
            $post = $GLOBALS['post'];
        }
    
        $extension = '';
        if (preg_match( '/^.*?\.(\w+)$/', get_attached_file($post->ID), $matches ) ) {
		    $extension = esc_html(strtoupper( $matches[1]));
		} else { 
			$extension = strtoupper(str_replace( 'image/', '', get_post_mime_type($post->ID)));
        }
        return $extension;
    }
}
