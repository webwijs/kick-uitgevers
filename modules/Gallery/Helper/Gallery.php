<?php

namespace Module\Gallery\Helper;

use Webwijs\System\AbstractFileSystem;
use Webwijs\Util\Strings;

class Gallery
{
    /**
     * A unique number for this gallery.
     *
     * @var int
     */
    private static $galleryNo = 0;

    /**
     * Displays a gallery containing images from a specific gallery.
     *
     * @param array|null $args (optional) arguments to change what is displayed.
     * @param WP_Post|null $post (optional) the post for which to retrieve a gallery.
     * @return string an HTML representation of a gallery.
     */
    public function gallery($args = null, $post = null, $return = 'gallery')
    {
        if ($post === null || $post == '') {
            $post = $GLOBALS['post'];
        }
    
        $defaults = array(
            'relation_key' => "gallery",
            'template'     => "partials/{$post->post_type}/gallery/list.phtml",
        );
        $args = array_merge($defaults, (array) $args);

        // get attachment ids associated with post.
        $attachmentIds = (class_exists('\Module\Gallery\Post')) ? \Module\Gallery\Post::getAttachmentIds($args['relation_key'], $post) : null;
        
        if($return == 'gallery'){
            // locate the correct template.
            $template = $this->locateTemplate($args['template']);
            
            return $this->view->partial($template, array('attachment_ids' => $attachmentIds,'title' => false, 'class_name' => sprintf('gallery-container-%d', ++self::$galleryNo)));
        }
        else if($return == "title"){
            // locate the correct template.
            $template = $this->locateTemplate($args['template']);
            
            return $this->view->partial($template, array('attachment_ids' => $attachmentIds,'title' => true, 'class_name' => sprintf('gallery-container-%d', ++self::$galleryNo)));
        }
        else{
            return $attachmentIds;
        }
            
    }
    
    /**
     * Returns a fully qualified path for the given template.
     *
     * @param string $template the template whose presence will be tested.
     * @param string $default the default template which will be returned on failure.
     * @return string a fully qualified path to the given template, or the default value on failure.
     * @throws InvalidArgumentException if the first argument is not of type 'string'.
     */
    private function locateTemplate($template, $default = 'partials/gallery/list.phtml')
    {
        if (!is_string($template)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string as first argument; received "%s" instead',
                __METHOD__,
                (is_object($template)) ? get_class($template) : gettype($template)
            ));
        }

        $fileSystem = AbstractFileSystem::getFileSystem();
        if (!$fileSystem->isAbsolute($template)) {
            $template = Strings::addTrailing(STYLESHEETPATH, DIRECTORY_SEPARATOR) . ltrim($template, DIRECTORY_SEPARATOR);
        }


        return (file_exists($template)) ? $template : (string) $default;
    }
}

