<?php

namespace Module\Gallery\Helper;

class RowBuilder
{
    public function rowBuilder($posts, $args = null)
    {
        if(is_object($posts)) {
            $posts = array($posts);
        }
        
        $defaults = array(
            'meta_key' => 'gallery',
            'counter' => 0
        );
        $options = array_merge($defaults, (array) $args);

        $counter = intval($options['counter']);
        $rows = '';
        if(is_array($posts) || ($posts instanceof \Traversable)) {
            foreach($posts as $post) { 
                $counter++;
                $class = ($counter % 2 == 0) ? '' : 'alternate-color';
                $rows .= $this->view->partial('partials/helpers/row.phtml', array('post' => $post, 'options' => $options, 'classname' => $class));
            }
        }
        return $rows;
    }
}
