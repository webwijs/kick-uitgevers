<?php

namespace Module\Gallery\Metabox;

abstract class Metabox
{
    static public $id;
    static public $title;
    static public $context = 'side'; // 'normal', 'advanced', or 'side'
    static public $priority = 'low'; // 'high', 'core', 'default' or 'low')

    public $postType;
    public function __construct($postType, $settings)
    {
        $this->postType = $postType;
        foreach ($settings as $name => $value) {
            // if possible use a mutator method.
            $method = sprintf('set%s', $this->normalizeClass($name));
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            } else {
                static::$$name = $value;
            }
        }
        $this->init();
    }
    
    public function init()
    {}
    
    public abstract function display($post);
    
    public abstract function save($postId);
    
    public function doSave($postId)
    {
        if ($this->_canSave($postId)) {
            $this->save($postId);
        }
    }
    
    protected function _canSave($postId)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return false;
        }
        if (!current_user_can('edit_post', $postId)) {
            return false;
        }
        if (get_post_type($postId) != $this->postType) {
            return false;
        }
        return true;
    }
    
    /**
     * Returns true if the given property exists and is static,
     * false otherwise.
     *
     * @param string the property to check.
     * @return bool returns true if the property exists and 
     *              is static, false otherwise.
     */
    protected function isStatic($name)
    {
        if (!is_string($name)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($name) ? get_class($name) : gettype($name))
            ));
        }
        
        if (property_exists($this, $name)) {

            return $prop->isStatic();
        }
        
        return false;
    }
    
    /**
     * Normalize the given name so that whitespace, underscores or hyphens
     * are removed and capitalize the subsequent character following any of
     * these characters.
     *
     * @param string $name the name that will be normalized.
     * @return string a normalized string.
     */
    protected function normalizeClass($name)
    {
        if (!is_string($name)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($name) ? get_class($name) : gettype($name))
            ));
        }
        
        // array containing parts of the original name.
        $parts = explode('_', str_replace(array(' ', '-'), '_', $name));
        
        /*
         * capitalize first character of each newly formed word and glue
         * all words together making one single word.
         */
        $normalized = '';
        if (is_array($parts)) {
            $normalized = implode('', array_map('ucfirst', $parts));
        }
        
        return $normalized;
    }
    
    /**
     * Denormalize the given name by separating words at an uppercase
     * character and placing hypens between the newly formed words. 
     *
     * @param string $name the name that will be denormalized.
     * @return string a denormalized string.
     */
    protected function denormalizeClass($name)
    {
        if (!is_string($name)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($name) ? get_class($name) : gettype($name))
            ));
        }
        
        // array containing parts of the original name.
        $parts = preg_split('/(?=[A-Z])/', $name, -1, PREG_SPLIT_NO_EMPTY);

        /*
         * lowercase all alphabetic characters and place hypens between 
         * the newly formed words.
         */
        $denormalized = '';
        if (is_array($parts)) {
            $denormalized = implode('_', array_map('strtolower', $parts));
        }
        
        return $denormalized;
    }
    
    public static function register($types, $settings = null)
    {
        $types = (array) $types;
        $settings = array_merge(
            array('id' => static::$id, 'title' => static::$title, 'context' => static::$context, 'priority' => static::$priority),
            (array) $settings
        );
        foreach ($types as $type) {
            $metabox = new static($type, $settings);
            add_meta_box(static::$id, static::$title, array(&$metabox, 'display'), $type, $settings['context'], $settings['priority']);
            add_action('save_post', array(&$metabox, 'doSave'));
            unset($metabox);
        }
    }
}
