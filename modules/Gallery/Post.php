<?php

namespace Module\Gallery;

class Post
{
    public static function queryAttachments($args = null, $key = null, $post = null)
    {
        global $wpdb;
        if (is_null($post)) {
            $post = $GLOBALS['post'];
        }
        $ids = self::getAttachmentIds($key, $post);
        if (!empty($ids)) {
            $defaults = array(
                'post__in' => $ids,
                'post_type' => 'attachment',
                'post_status' => 'inherit',
                'nopaging' => true,
            );
            $args = wp_parse_args($args, $defaults);
            if ($args['post_type'] == 'public') {
                $args['post_type'] = array_diff(get_post_types(array('public' => true)), array('attachment'));
            }
            if (empty($args['orderby']) && empty($args['custom_orderby'])) {
                $args['custom_orderby'] = 'FIELD(' . $wpdb->posts . '.ID, ' . implode(', ', array_map('absint', $ids)) . ')';
            }
            query_posts($args);
            return true;
        }
        return false;
    }

    public static function getAttachments($args = null, $key = null, $post = null)
    {
        global $wpdb;
        if (is_null($post)) {
            $post = $GLOBALS['post'];
        }
        $ids = self::getAttachmentIds($key, $post);
        if (!empty($ids)) {
            $defaults = array(
                'post__in' => $ids,
                'post_type' => 'attachment',
                'post_status' => 'inherit',
                'nopaging' => true,
            );
            $args = wp_parse_args($args, $defaults);
            if ($args['post_type'] == 'public') {
                $args['post_type'] = array_diff(get_post_types(array('public' => true)), array('attachment'));
            }
            if (empty($args['orderby']) && empty($args['custom_orderby'])) {
                $args['custom_orderby'] = 'FIELD(' . $wpdb->posts . '.ID, ' . implode(', ', array_map('absint', $ids)) . ')';
            }
            
            return get_posts($args);
        }
        return false;
    }

    public static function getAttachmentIds($key = null, $post = null)
    {
        global $wpdb;
        if (is_null($post)) {
            $post = $GLOBALS['post'];
        }
        $keyCondition = '';
        if (!empty($key)) {
            $keyCondition = $wpdb->prepare(' AND relation_key = %s', $key);
        }

        $subQuery = <<<SQL
            SELECT attachment_id
            FROM {$wpdb->prefix}gallery
            WHERE post_id = %d {$keyCondition}
            ORDER BY sort_order ASC
SQL;

        $subQuery = $wpdb->prepare($subQuery, $post->ID, $key);
        $ids = $wpdb->get_col($subQuery);
        return $ids;
    }
    
    public static function updateAttachment($postId, $posts, $key)
    {
        global $wpdb;

        $posts = (array) $posts;
        $posts = array_map('intval', $posts);

        $deleteSql = <<<SQL
            DELETE FROM {$wpdb->prefix}gallery
            WHERE (post_id = %d OR attachment_id = %d) AND relation_key = %s
SQL;

        $deleteSql = $wpdb->prepare($deleteSql, $postId, $postId, $key);        
        if (!empty($posts)) {
            $notIn = implode(', ', array_keys($posts));
            $deleteSql .= ' AND post_id NOT IN (' . $notIn . ') AND attachment_id NOT IN (' . $notIn . ')';
        }
        $wpdb->query($deleteSql);

        foreach ($posts as $attachmentId => $sort_order) {
            $sql = <<<SQL
                INSERT INTO {$wpdb->prefix}gallery
                SET post_id = %d, attachment_id = %d, relation_key = %s, sort_order = %d
                ON DUPLICATE KEY UPDATE sort_order = %d
SQL;
            $sql = $wpdb->prepare($sql, $postId, $attachmentId, $key, $sort_order, $sort_order);
            $wpdb->query($sql);
        }
    }
    
    public static function addAttachment($postId, $attachmentId, $key = null, $sort_order = 0)
    {
        global $wpdb;
        
        $postId = intval($postId);
        $attachmentId = intval($attachmentId);
        $key = strip_tags($key);
        $sort_order = intval($sort_order);
        
        $sql = <<<SQL
            INSERT INTO {$wpdb->prefix}gallery 
            SET post_id = %d, attachment_id = %d, relation_key = %s, sort_order = %d
SQL;
        $sql = $wpdb->prepare($sql, $postId, $attachmentId, $key, $sort_order);
        return $wpdb->query($sql);
    }
}
