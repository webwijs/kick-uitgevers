(function($) {
    $.fn.gallery = function (options) {
        var self = this;
        var defaults = {
            add:    '.add-gallery-image',
            remove: '.remove-gallery-image',
            table: '.webwijs-gallery-table',
            settings: {
                ajaxurl: null,
                action: 'add-gallery-image',
                key: 'gallery'
            }
        };
        var options = $.extend(true, {}, defaults, options);
        var $container, $table;
        var mediaDialog;
        
        /**
         * Constructor function which is invoked everytime a element is used with this plugin.
         *
         * @param container the actual element used by this plugin.
         */
        this.init = function(container) {
            $container = $(container);

            // handler that removes the selected image from the gallery.
            $container.on('click', options.remove, function(event) {
                var $row = $(this).parents('tr');
                if($row.length > 0) {
                    $row.remove(); 
                }
                
                // remove the table if no it holds no rows.
                $table = $(options.table, $container);
                if($('tbody tr', $table).length == 0) {
                    $table.remove();
                }

                // prevent default action.
                return false;
            });

			// handler that opens the content media manager to the 'featured image' tab.
			$container.on( 'click', options.add, function(event) {
			    // prevent default action.
			    event.preventDefault();
			
			    // reopen existing dialog.
			    if (mediaDialog) {
			        mediaDialog.open();
			        // no need to continue.
			        return;
		        }
		        
		        // create a new media dialog.
                mediaDialog = wp.media.frames.file_frame = wp.media({
                    title: $(this).data('uploader_title'),
                    button: {
                        text: $(this).data('uploader_button_text')
                    },
                    multiple: true
                });
        
                // when a file is selected, grab the URL and set it as the text field's value.
                mediaDialog.on('select', function() {
                    var selection = mediaDialog.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        
                        // perform ajax request.
                        self.ajax({thumbnail_id: attachment.id});
                    });
                });
 
                // open media dialog.
                mediaDialog.open();
			});
        };
        
        this.ajax = function(args) {
            args = (typeof args === 'object' && args.hasOwnProperty('data')) ? args : { data: args };
            var settings = $.extend(true, {}, args || {}, {
			    type: 'POST',
                url:  options.settings.ajaxurl,
                data: {
                    action: options.settings.action,
                    relation_key: options.settings.key,
                    counter: 0
                }
		    });
		    
            $table = $(options.table, $container);
            if($table.length > 0) {
                settings.data.counter = $('tbody tr', $table).length;
            }

			$.ajax(settings).done(function(response) {
                if(response != 0) {
                    // create a new table if the old one has been removed.
                    if($table.length == 0) {
                        $table = self.createTable();
                    }
                    $('tbody', $table).append(response);
                }
			    $(self).trigger('done');
			});
        }
        
        /**
         * Creates an empty table to which can hold images for our gallery.
         *
         * @return object returns the new table element.
         */
        this.createTable = function()
        {
            var $table = $(document.createElement('table')).addClass('webwijs-gallery-table');
            $table.append(
                $(document.createElement('tbody'))
            );
            $('.inside', $container).append($table);
            
            return $table;
        }

        /**
         * Iterate over jQuery elements and call the constructor method for this plugin.
         *
         * @param index the index of the current element
         * @param container the actual element that is used by this plugin.
         */
        this.each(function(index, container) {
            self.init(container);
        });

        /**
         * Object that can be used to make private functions accessible for the outside world.
         */
        return {
            ajax: self.ajax
        };
    }
})(jQuery)
