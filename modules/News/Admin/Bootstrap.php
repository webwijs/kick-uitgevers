<?php

namespace Module\News\Admin;

use Webwijs\AbstractBootstrap;
use Theme\Admin\Controller\SettingsController;

use Theme\Admin\Metabox\News;

/**
 * The admin bootstrap of the News module
 *
 * @author Leo Flapper
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap
{

	protected function _initSettings()
	{
		$builder = SettingsController::getBuilder();
		$builder->group('hide')
                ->add('theme_hide_news_taxonomy', 'checkbox', array('label' => __('Nieuws categorieen')));

		$builder->group('page')
		        ->add('theme_page_news', 'postSelect', array('label' => __('Nieuwspagina')));
	}

    /**
     * Initializes the metaboxes for the News module
     * @return void
     */
    protected function _initMetaboxes()
    {
        News::register('news');
    }

}
