<?php

namespace Module\News\Cpt;

use Webwijs\Cpt;

/**
 * The News custom post type
 *
 * @author Joris Wagter
 * @version 1.0.0
 */
class News extends Cpt
{
    /**
     * The post type name
     * @var string $type the post type name
     */
    public $type = 'news';

    /**
     * Sets the labels and settings for the custom post type
     * @return void
     */
    public function init()
    {
        $this->labels = array(
            'name'                  => __('Berichten', 'news'),
            'singular_name'         => __('Bericht', 'news'),
            'add_new'               => __('Nieuw bericht', 'news'),
            'add_new_item'          => __('Nieuw bericht', 'news'),
            'edit_item'             => __('Bericht bewerken', 'news'),
            'new_item'              => __('Nieuw bericht', 'news'),
            'view_item'             => __('Bericht bekijken', 'news'),
            'search_items'          => __('Berichten zoeken', 'news'),
            'not_found'             => __('Geen berichten gevonden', 'news'),
            'not_found_in_trash'    => __('Geen berichten gevonden', 'news'),
            'menu_name'             => __('Nieuws', 'news')
        );

        $this->settings = array(
            'rewrite'       => false,
            'hierarchical'  => false,
            'public'        => true,
            'show_ui'       => true,
			'menu_icon'		=> 'dashicons-format-aside',
			'menu_position' => 5,
            'supports'      => array('title', 'editor', 'excerpt', 'thumbnail')
        );

		// only register taxonomy when option to hide them is not activated
		if (!get_option('theme_hide_news_taxonomy')) {
			add_action('init', array($this, 'registerCategory'));
		}
    }

	/**
     * Create a new taxonomy for this custom post type.
     *
     * @return void
     */
    public function registerCategory()
    {
        $labels = array(
            'name'              => __( 'Categorieen', 'theme'),
            'singular_name'     => __( 'Categorie', 'theme'),
            'search_items'      => __( 'Categorie zoeken', 'theme'),
            'all_items'         => __( 'Alle categorieen', 'theme'),
            'parent_item'       => __( 'Overkoepelend categorie', 'theme'),
            'parent_item_colon' => __( 'Overkoepelend categorie', 'theme'),
            'edit_item'         => __( 'Wijzig categorie', 'theme'),
            'update_item'       => __( 'Categorie bijwerken', 'theme'),
            'add_new_item'      => __( 'Nieuwe categorie toevoegen', 'theme'),
            'new_item_name'     => __( 'Nieuwe categorie', 'theme'),
            'menu_name'         => __( 'Categorieen', 'theme'),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => false,
            'rewrite'           => false,
        );

        register_taxonomy('news_category', $this->type, $args);
    }

    /**
     * Registers the custom post type
     * @param  array $options the options for registering the custom post type
     * @return void
     */
    public static function register($options = null)
    {
        new self($options);
    }

    /**
     * Default query function for retrieving post of the custom post type
     * @param  array $args array of arguments for retrieving the posts
     * @return WP_Query $posts WP Query object containing the retrieved posts
     */
    public static function queryPosts($args = null)
    {
        $defaults = array(
            'post_type' => 'news',
            'orderby' => 'date',
            'order' => 'DESC',
        );
        $args = array_merge( $defaults, (array) $args);

        return query_posts($args);
    }

    /**
     * Returns the post type name
     * @return string $type the post type name
     */
    public function getType()
    {
        return $this->type;
    }
}
