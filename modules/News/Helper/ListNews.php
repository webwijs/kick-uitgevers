<?php

namespace Module\News\Helper;

use Webwijs\Post;
use Webwijs\Util\Arrays;

/**
 * Helper for displaying a list of news items
 *
 * @author Joris Wagter
 * @version 1.0.0
 */
class ListNews
{

	/**
	 * Function to list news items.
	 * Arguments can be used to filter the news or display the news in a certain template.
	 * @param  array $args array containing arguments
	 * @return string $output the html output of the retrieved news in a certain template, or null if no news items are found
	 */
    public function listNews($args = null)
    {
        $defaults = array(
            'queryArgs' => array(
                'post_type' => 'news',
                'nopaging' => true,
                'orderby' => 'date',
                'order' => 'DESC'
            ),
            'template' => 'partials/news/list.phtml',
        );
        $args = Arrays::addAll($defaults, (array) $args);

        $output = '';
        query_posts($args['queryArgs']);
        if (have_posts()) {
            $output = $this->view->partial($args['template']);
        }
        wp_reset_query();

        return $output;
    }

}
