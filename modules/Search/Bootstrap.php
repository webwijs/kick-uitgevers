<?php
namespace Module\Search;

use Webwijs\AbstractBootstrap;
use Webwijs\Module\AutoloadableInterface;
use Webwijs\Module\ModuleAwareInterface;
use Webwijs\Module\ModuleInfo;
use Webwijs\Shortcode\ViewHelper;

/**
 * The Search bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface, ModuleAwareInterface
{
    /**
     * An object containing information about this module.
     *
     * @var ModuleInfo
     */
    private static $module = null;

    /**
     * {@inheritDoc}
     */
    public function setModule(ModuleInfo $module)
    {
        self::$module = $module;
    }
    
    /**
     * Returns the {@link ModuleInfo} instance for this module.
     *
     * @return ModuleInfo object containing information about this module.
     */
    public static function getModule()
    {
        return self::$module;
    }
    
    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {	
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Search' => __DIR__,
                ),
            ),
        );
    }
    
    /**
     * Register shortcodes with WordPress.
     */
    protected function _initShortcode()
    {
        $helper = new ViewHelper();
        add_shortcode('search-results', array($helper, 'searchShortcode'));
    }
    
    /**
     * Register hooks for one or more WordPress actions.
     */
    protected function _initActions()
    {
        add_action('admin_menu', array('Module\Search\Action\Settings', 'addOptions')); 
    }
}
