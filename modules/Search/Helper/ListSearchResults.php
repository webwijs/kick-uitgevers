<?php

namespace Module\Search\Helper;

use Webwijs\Util\Arrays;

/**
 * The ListSearchResults returns a list of posts that matches with the specified arguments.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class ListSearchResults
{
    /**
     * Returns a list of posts that match with the specified search query.
     * Use this helper inside a template or partial as is illustrated in the example below:
     *
     * <code>
     *     $this->listSearchResults(array(
     *         'query' => array(
     *             's' => $this->searchQuery()
     *         )
     *     ));
     * </code>
     *
     * @param array $args (optional) array that determines which results are returned.
     * @return string the posts that were found for the specified search query.
     */
    public function listSearchResults(array $args = array())
    {
        $defaults = array(
            'query_args' => array(
                'post_types'     => array('news','page','product'),
                's'              => '',
                'posts_per_page' => '10', 
                'paged'          => get_query_var('paged'),
            ),
            'template' => 'partials/search/results.phtml',
            'vars'     => array(),
        );
        $args = Arrays::addAll($defaults, $args);
        
        query_posts($args['query_args']);
        
        $template = 'partials/search/no-results.phtml';
        if (have_posts()) {
            $template = $args['template'];
        }
        
        $output = $this->view->partial($template, array_merge($args['vars'], array('query' => $args['query_args']['s'])));
        
        wp_reset_query();
        
        return $output;
    }
}
