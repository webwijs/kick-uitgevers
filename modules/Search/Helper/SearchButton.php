<?php

namespace Module\Search\Helper;

use SplFileInfo;

use Module\Search\Bootstrap;

use Webwijs\Dom\ElementInterface;
use Webwijs\Dom\HtmlElementBuilder;
use Webwijs\Dom\TextElement;

/**
 * The SearchButton returns a search button which can be used in conjunction with a search form.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class SearchButton
{
    /**
     * Returns a search button which can be used to toggle the visiblity of the specified form.
     * Use this helper inside a template or partial as is illustrated in the example below:
     *
     * <code>
     *     $form   = $this->searchForm();
     *     $button = $this->searchButton($form);
     * </code>
     * 
     *
     * @param array $args (optional) additional argument to change how the search button operates.
     * @return string the HTML output of the sarch button.
     */
    public function searchButton(ElementInterface $form)
    {
        add_action('wp_footer', array($this, 'enqueueScripts'));
        
        // build anchor
        $builder = new HtmlElementBuilder('a');
        $builder->attributes(array(
                        'href'  => '#',
                        'title' => __('Zoeken'),
                        'class' => 'toggler search-button'
                  ))
                ->child(function($builder) {
                        // build span
                        return $builder->tag('span')
                                       ->child(new TextElement(__('Zoeken')))
                                       ->build();
                  });
                  
        $button = $builder->build();
        if ($form->hasAttribute('id')) {
            $button->addAttribute('data-element-id', $form->getAttribute('id'));
        } else if ($form->hasAttribute('class')) {
            $button->addAttribute('data-element-class', $form->getAttribute('class'));
        }
        
        return $button;
    }
    
    /**
     * Enqueue the necessary scripts for this helper.
     */
    public function enqueueScripts()
    {    
        $module = Bootstrap::getModule();
        $script = new SplFileInfo(sprintf('%s/assets/js/jquery.toggler.js', get_template_directory()));
        if ($script->isReadable()) {
            $url = sprintf('%s/assets/js/jquery.toggler.js', get_template_directory_uri());            
        } else {
            $url = sprintf('%s/assets/js/jquery.toggler.js', $module->getUrl());
        }
        
        wp_enqueue_script('jquery-toggler', $url, array('jquery'), false, true);
    }
}
