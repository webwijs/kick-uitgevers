<?php

namespace Module\Search\Helper;

use Webwijs\Dom\HtmlElementBuilder;
use Webwijs\Dom\HtmlElement;
use Webwijs\Dom\TextElement;

/**
 * The SearchForm returns the search form.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class SearchFormHtml
{
    /**
     * The default form method.
     *
     * @var string
     */
    const DEFAULT_FORM_METHOD = 'post';

    /**
     * Returns a search form with can be used to find one or more posts.
     * Use this helper inside a template or partial as is illustrated in the example below:
     *
     * <code>
     *     $form = $this->searchForm(array(
     *                 'value'  => $this->searchQuery(),
     *                 'method' => 'post'
     *             ));
     * </code>
     *
     * @param array $args (optional) arguments used to build the form.
     * @return ElementInterface object that represents the html element.
     */
    public function searchFormHtml(array $args = array())
    {
        $defaults = array(
            'value'  => '',
            'action' => get_option('module_page_search'),
            'method' => 'get',
			'template' => 'partials/search/form.phtml'
        );
        $args = array_merge($defaults, (array) $args);

		$formArgs = (object) [
			'class'  => 'form',
			'role'   => 'search',
			'method' => $this->getMethod($args['method']),
			'action' => $this->getAction($args['action']),
		];
		$searchInput = (object) [
			'class'       => 'search-input',
			'type'        => 'text',
			'name'        => 'search',
			'value'       => esc_attr($args['value']),
			'placeholder' => __('Zoeken'),

		];
		$searchSubmit = (object) [
			'class' => 'search-submit',
			'type'  => 'submit',
		];
		$output = $this->view->partial($args['template'], ['form' => $formArgs, 'input' => $searchInput, 'submit' => $searchSubmit]);
        // build form
        // $builder = new HtmlElementBuilder('form');
        // $builder->attributes(array(
        //             'class'  => 'form',
        //             'role'   => 'search',
        //             'method' => $this->getMethod($args['method']),
        //             'action' => $this->getAction($args['action']),
        //           ))
        //         ->child(function($builder) use ($args) {
        //                 $input = new HtmlElement('input');
        //                 $input->addAttributes(array(
        //                     'class'       => 'search-input',
        //                     'type'        => 'text',
        //                     'name'        => 'search',
        //                     'value'       => esc_attr($args['value']),
        //                     'placeholder' => __('Zoeken'),
        //                 ));
		//
        //                 // build input container
        //                 return $builder->tag('div')
        //                                ->attribute('class', 'search-input-container')
        //                                ->child($input)
        //                                ->build();
        //           })
        //         ->child(function($builder) {
        //                 $span = new HtmlElement('span');
        //                 $span->addChild(new TextElement(__('Zoeken')));
		//
        //                 // build button
        //                 return $builder->tag('button')
        //                                ->attributes(array(
        //                                     'class' => 'search-submit',
        //                                     'type'  => 'submit',
        //                                  ))
        //                                ->child($span)
        //                                ->build();
		//
        //           });

        // $container = new HtmlElement('div');
        // $container->addAttribute('class', 'search-form');
        // $container->addChild($builder->build());

        return $output;
    }

    /**
     * Returns a valid form method.
     *
     * @param string $method the method to be tested for validity.
     * @return string the given method if proven valid, otherwise the default method is returned.
     * @throws InvalidArgumentException if the given argument is not of type 'string'.
     */
    private function getMethod($method)
    {
        if (!is_string($method)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($method) ? get_class($method) : gettype($method))
            ));
        }

        $method = strtolower($method);
        return (in_array($method, array('get', 'post'))) ? $method : DEFAULT_FORM_METHOD;
    }

    /**
     * Returns a sanitized url to which the form will submit it's data.
     *
     * It's asummed that a numeric value corresponds to a post id and means that this
     * method will call the {@link get_permalink($id, $leavename)} function to retrieve
     * the permalink associated with the given post.
     *
     * @param string|int $url a string representation of a url, or a post id.
     */
    private function getAction($url)
    {
        if (is_numeric($url)) {
            $url = get_permalink((int) $url);
        }

        return (is_string($url)) ? esc_url($url) : '';
    }
}
