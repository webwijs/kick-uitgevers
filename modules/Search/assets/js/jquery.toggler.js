(function($, window, document, undefined) {
    "use strict";
    
    if (typeof String.prototype.addTrailing !== 'function') {
        /**
         * Append the specified character to the string.
         *
         * @param {string} char the character to append.
         * @return {string} a string with the specified character appended to it.
         */
        String.prototype.addTrailing = function (char) {
            var self = this;
            if (self.charAt(0) == char) {
                self = self.slice(1);
            }
            
            return char + self;
        }
    }
    
    $('body').on('click', '.toggler', function(event) {
        var className = 'active';
        if (typeof $(this).data('element-id') === 'string') {
            var selector = $(this).data('element-id').addTrailing('#');
        } else if (typeof $(this).data('element-class') === 'string') {
            var selector = $(this).data('element-class').addTrailing('.');
        } else {
            var selector = '';
        }

        var $element = $(selector).eq(0).toggleClass(className);
        if ($element.hasClass(className)) {
            $(this).addClass(className);
        } else {
            $(this).removeClass(className);
        }
        
        event.preventDefault();
    });
    
})(jQuery, this, this.document);
