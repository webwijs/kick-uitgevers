<?php

namespace Module\Share\Helper;

use Module\Share\Bootstrap;
use Module\Share\Type\TypeInterface;

use Webwijs\Util\Arrays;

class ShareThis
{
    /**
     * Displays a collection of share this links.
     * 
     * @param array $args (optional) arguments to change the output of this helper.
     * @param WP_Post|null $post (optional) the post to share.
     * @return string the html output of this helper.
     * @link https://developers.facebook.com/docs/plugins/share-button/
     * @link https://dev.twitter.com/docs/tweet-button
     */
    public function shareThis($args = null, $post = null)
    {
        if (!is_object($post)) {
            $post = $GLOBALS['post'];
        }
    
        $defaults = array(
            'sharer' => array(
                'url'   => get_permalink($post->ID),
                'title' => get_the_title($post->ID),             
            ),
            'template' => 'partials/sharethis/sharethis.phtml',
            'vars' => array(),
        );
        $args = Arrays::addAll($defaults, (array) $args);

        add_action('wp_enqueue_scripts', array($this, 'enqueueScripts'));

        // filter active plugins.
        $plugins = Bootstrap::getPlugins();
        $plugins = $plugins->filter(function($plugin) {
            return $plugin->isActive();
        });
        
        // create a collection of HTMLElements.
        $elements = array();
        foreach ($plugins as $plugin) {
            $elements[] = $plugin->getElement($args['sharer']);
        }

        return $this->view->partial($args['template'], array_merge($args['vars'], array('elements' => $elements)));
    }

    /**
     * Enqueue necessary scripts.
     */
    public function enqueueScripts()
    {
        wp_enqueue_script('jquery.share', get_bloginfo('stylesheet_directory') . '/modules/Share/assets/js/jquery.share.js', array('jquery'), false, true);
        wp_enqueue_style('module.share', get_bloginfo('stylesheet_directory') . '/modules/Share/assets/css/share.css');
    }
}
