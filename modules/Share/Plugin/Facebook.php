<?php

namespace Module\Share\Plugin;

use Theme\Admin\Controller\Form\FormBuilderInterface;

use Webwijs\Dom\HtmlElementBuilder;

/**
 * The Facebook plugin is a concrete implementation of the {@link PluginInterface} and allows a user
 * to share a web page using Facebook.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class Facebook implements PluginInterface
{
    /**
     * {@link inheritDoc}
     */
    public function isActive()
    {
        return (get_option('theme_share_facebook') === '1');
    }

    /**
     * {@link inheritDoc}
     */
    public function getElement(array $args = array())
    {
        $defaults = array(
            'url' => ''
        );
        $args = array_merge($defaults, $args);

        // build anchor
        $builder = new HtmlElementBuilder('a');
        $builder->attributes(array(
                      'href' => sprintf('//facebook.com/sharer.php?u=%1$s', urlencode($args['url'])),
                      'title' => __('Deel deze informatie op Facebook'),
                      'class' => 'popup facebook',
                      'data-width' => 575,
                      'data-height' => 335
                  ))
                ->child(function($builder) {
                        // build icon
                        return $builder->tag('i')
                                       ->attribute('class', 'fa fa-facebook')
                                       ->build();
                  });

        return $builder->build();
    }

    /**
     * {@link inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->group('share', __('Delen'))
                ->add('theme_share_facebook', 'checkbox', array('label' => __('Delen via Facebook')));
    }
}
