<?php

namespace Module\Share\Plugin;

use Theme\Admin\Controller\Form\FormBuilderInterface;

use Webwijs\Dom\HtmlElementBuilder;

/**
 * The Mail plugin is a concrete implementation of the {@link PluginInterface} and allows a user
 * to share a web page through an e-mail.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class Mail implements PluginInterface
{
    /**
     * {@link inheritDoc}
     */
    public function isActive()
    {
        return (get_option('theme_share_email') === '1');
    }

    /**
     * {@link inheritDoc}
     */
    public function getElement(array $args = array())
    {
        $defaults = array(
            'url' => ''
        );
        $args = array_merge($defaults, $args);

        // build anchor
        $builder = new HtmlElementBuilder('a');
        $builder->attributes(array(
                      'href'  => 'javascript:;',
                      'title' => __('Verstuur via e-mail'),
                      'class' => 'mail'
                  ))
                ->child(function($builder) {
                        // build icon
                        return $builder->tag('i')
                                       ->attribute('class', 'fa fa-envelope-square ion-android-mail')
                                       ->build();
                  });

        return $builder->build();
    }

    /**
     * {@link inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->group('share', __('Delen'))
                ->add('theme_share_email', 'checkbox', array('label' => __('Delen via E-mail')))
                ->add('theme_share_email_subject', 'text', array('label' => __('E-mail onderwerp')));
    }
}
