<?php

namespace Module\Share\Plugin;

use Theme\Admin\Controller\Form\FormBuilderInterface;

/**
 * The PluginInterface defines the method required to display a share icon on the website.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
interface PluginInterface
{
    /**
     * Returns true if this share plugin is active.
     *
     * @return bool true if active, otherwise false.
     */
    public function isActive();
    
    /**
     * Returns an {@link ElementInterface} associated with this share plugin.
     *
     * @param array $args (optional) additional arguments to create the HTML element.
     * @return ElementInterface the HTML element.
     */
    public function getElement(array $args = array());

    /**
     * Add fields to the specified form builder.
     *
     * @param FormBuilderInterface $builder the form builder.
     */
    public function buildForm(FormBuilderInterface $builder);
}
