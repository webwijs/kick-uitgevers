jQuery(function($) {
    $('.sharethis').on('click', '.popup', function(event) {
        // display popup screen.
        var width  = $(this).data('width'),
            height = $(this).data('height'),
            left   = ($(window).width()  - width)  / 2,
            top    = ($(window).height() - height) / 2,
            url    = this.href,
            opts   = 'status=1' +
                     ',width='  + width  +
                     ',height=' + height +
                     ',top='    + top    +
                     ',left='   + left;
        window.open(url, 'popupScreen', opts);
        
        // prevent default action.
        event.preventDefault();
    }); 
});
