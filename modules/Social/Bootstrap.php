<?php
namespace Module\Social;

use Module\Social\Plugin\Facebook;
use Module\Social\Plugin\Twitter;
use Module\Social\Plugin\LinkedIn;
use Module\Social\Plugin\YouTube;
use Module\Social\Plugin\Pinterest;
use Module\Social\Plugin\Bitbucket;

use Webwijs\AbstractBootstrap;
use Webwijs\Collection\ArrayList;
use Webwijs\Module\AutoloadableInterface;

/**
 * The Social bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface
{
    /**
     * A collection of socials plugins.
     * 
     * @var ListInterface
     */
    private static $plugins = null;

    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Social' => __DIR__,
                ),
            ),
        );
    }
    
    /**
     * Initialize social plugins.
     */
    protected function _initPlugins()
    {
        self::$plugins = new ArrayList(array(
            new Facebook(),
            new Twitter(),
            new LinkedIn(),
            new YouTube(),
            new Pinterest(),
        ));
    }
    
    /**
     * Register hooks for one or more WordPress actions.
     */
    protected function _initActions()
    {
        add_action('admin_menu', array('Module\Social\Action\Settings', 'addOptions')); 
    }
    
    /**
     * Returns a collection of social plugins.
     *  
     * @return ListInterface a collection of social plugins.
     */
    public static function getPlugins()
    {
        return self::$plugins;
    }
}
