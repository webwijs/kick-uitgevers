<?php

namespace Module\Social\Helper;

use Module\Social\Bootstrap;

class Social
{
    /**
     * Displays a collection of social media icons.
     *
     * @param array $args (optional) arguments to change the output of this helper.
     * @return string the html output of this helper.
     */
    public function social(array $args = array())
    {
        $defaults = array(
            'template' => 'partials/social/list.phtml',
            'vars' => array(),
        );
        $args = array_merge($defaults, $args);

        // filter active plugins.
        $plugins = Bootstrap::getPlugins();
        $plugins = $plugins->filter(function($plugin) {
            return $plugin->isActive();
        });
        
        // create a collection of HTMLElements.
        $elements = array();
        foreach ($plugins as $plugin) {
            $elements[] = $plugin->getElement();
        }

        return $this->view->partial($args['template'], array_merge($args['vars'], array('elements' => $elements)));
    }
}
