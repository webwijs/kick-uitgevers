<?php

namespace Module\Social\Plugin;

use Theme\Admin\Controller\Form\FormBuilderInterface;

use Webwijs\Dom\HtmlElementBuilder;

/**
 * This plugin will display a Twitter icon if and only if a Twitter webpage is provided.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class Twitter implements PluginInterface
{
    /**
     * {@inheritDoc}
     */
    public function isActive()
    {
        return (get_option('theme_social_twitter', '') !== '');
    }

    /**
     * {@inheritDoc}
     */
    public function getElement(array $args = array())
    {
        $url = get_option('theme_social_twitter', '');

        // build anchor
        $builder = new HtmlElementBuilder('a');
        $builder->attributes(array(
                      'href'   => esc_url($url),
                      'title'  => __('Like us on Twitter'),
                      'class'  => 'social twitter',
                      'target' => '_blank',
                  ))
                ->child(function($builder) {
                        // build icon
                        return $builder->tag('i')
                                       ->attribute('class', 'fa fa-twitter-square')
                                       ->build();
                  });

        return $builder->build();
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->group('socialmedia', __('Sociale media'))
                ->add('theme_social_twitter', 'text', array('label' => __('Twitter pagina')));
    }
}
