function styleSwitcher($elem){
  var $select = jQuery($elem);
  var $parent = $select.parents('.widget-content');
  var style = $parent.find('input:checked').val();
  //console.log(style);
  if(style == 'image'){
    //console.log('if image');
    $parent.find('.icon-dropdown').addClass('hidden-option');
    $parent.find('.icon-container').addClass('hidden-option');
    $parent.find('.image-dropdown').removeClass('hidden-option');
    $parent.find('.image-container').removeClass('hidden-option');
  }
  else{
    //console.log('else icon');
    $parent.find('.icon-dropdown').removeClass('hidden-option');
    $parent.find('.icon-container').removeClass('hidden-option');
    $parent.find('.image-dropdown').addClass('hidden-option');
    $parent.find('.image-container').addClass('hidden-option');
  }

}
(function($, window, document, undefined) {
    styleSwitcher($('#widgets-right .usp-style-switcher'));
    $('#widgets-right').on('change ajaxComplete load', function(){
      $('.widget').each(function(key, value){
        styleSwitcher($('.usp-style-switcher', value));
      });
    });

    $('#widgets-right').on('change', '.widget-icon-dropdown-field', function(e) {
        var $select = $(this);
        var $parent = $select.parents('.widget-content');

        var $container = $parent.find('.icon-container').addClass('loading');
        var optionSelected = $("option:selected", $select);
        var valueSelected = $select.val();
        $container.find('[data-icon="true"]').attr('class', "fa "+valueSelected);
        $container.removeClass('loading');
      });

    $('#widgets-right').on('change', '.widget-image-dropdown-field', function(e) {
        var $select = $(this);
        var $parent = $select.parents('.widget-content');

        var $container = $parent.find('.image-container').addClass('loading');
        if (0 === $container.length) {
            $container = $(document.createElement('div')).prependTo($parent).addClass('image-container loading');
        }

        if (typeof ajaxurl === 'string' && ajaxurl.length) {
            var data = {
                action: 'get_attachment_url',
                attachment_id: $select.val()
            };
          //  console.log(ajaxurl, data);
            $.post(ajaxurl, data, function(src) {
                if (typeof src === 'string' && src.length) {
                    $(document.createElement('img')).attr("src", src).appendTo($container.empty());
                } else {
                    $container.remove();
                }
            }).done(function() {
                $container.removeClass('loading');
            });
        }
    });
})(jQuery, this, this.document);
