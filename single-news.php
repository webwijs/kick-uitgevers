<?php
/*
 * Layouts: one-column
 */
?>
<?php the_post(); ?>

<?php 
	global $post;
	$parent_title = get_the_title( get_option('theme_page_news') );
?>



<section class="pagetop">
	<div class="row">
		<div class="small-12 column center">
			<h3 class="subtitle red"><?php echo $parent_title; ?></h3>
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
</section>

<section class="maincontent page-<?php echo get_post_type() ?>">
	<div class="row align-center">
		<div class="small-12 medium-8 column">
			<?php echo $this->partial('partials/news/singular.phtml') ?>
		</div>
	</div>
</section>

<div class="share">
	<?php echo $this->shareThis(); ?>
</div>

<section class="border">
	<div class="row">
		<div class="small-12 column">
			<div class="borderdiv"></div>
		</div>
	</div>
</section>

<section class="news page-<?php echo get_post_type() ?> topborder">
	<div class="row">
        <div class="small-12 columns">
            <h2 class="sectiontitle noborder">Meer actualiteiten</h2>
        </div>
    </div>
	<div class="post-list">
		<div class="row">
			<?php if($news = $this->selectedNews()): ?>
				<?php echo $news; ?>
			<?php else: ?>
				<?php 
			        $defaults = [
			            'template' => 'partials/news/list.phtml',
			            'queryArgs' => [
			                'posts_per_page' => 3,
			                'nopaging' => false
			            ]
			        ];
			        echo $this->listNews($defaults); 
			    ?>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="crumbs">
	<div class="row">
		<div class="small-12 column center">
			<?php echo $this->breadcrumbs() ?>
		</div>
	</div>
</section>

<?php echo $this->partial('partials/layout/pagebottom.phtml') ?>
