<?php
/**
 * Template Name: Checkout
 * Layouts: one-column
 */
?>
<?php the_post(); ?>

<section class="pagetop">
	<div class="row">
		<div class="small-12 column center">
			<h3 class="subtitle red"><?php the_title() ?></h3>
			<h1><?php echo get_post_meta(get_the_ID(),'_text_toptitle',true); ?></h1>
		</div>
	</div>
</section>

<section class="maincontent page-<?php echo get_post_type() ?>">
	<div class="row align-center">
		<div class="small-12 column">
			<?php echo $this->partial('partials/page/singular-simple.phtml') ?>
		</div>
	</div>
</section>

<section class="crumbs">
	<div class="row">
		<div class="small-12 column center">
			<?php echo $this->breadcrumbs() ?>
		</div>
	</div>
</section>

<?php echo $this->partial('partials/layout/pagebottom.phtml') ?>
