<?php
/**
 * Template Name: Contact
 * Layouts: one-column
 */
?>
<?php the_post(); ?>

<section class="pagetop">
	<div class="row">
		<div class="small-12 column center">
			<h3 class="subtitle red"><?php the_title() ?></h3>
			<h1><?php echo get_post_meta(get_the_ID(),'_text_toptitle',true); ?></h1>
		</div>
	</div>
</section>

<section class="maincontent page-<?php echo get_post_type() ?>">
	<div class="row">
		<div class="small-12 large-6 column">

			<div class="contactinfo">
				<h2>Zo kunt u ons bereiken</h2>
				<div class="row">
					<div class="small-6 column">
						<div class="wicon phone">
							<?php echo $this->phone(get_option('theme_company_phone')); ?>
						</div>
						<div class="wicon email">
							<a href="mailto:<?php echo get_option('theme_company_email'); ?>"><?php echo get_option('theme_company_email'); ?></a>
						</div>
					</div>
					<div class="small-6 column">
	                    <div class="wicon address">
	                        <?php echo str_replace(',','<br/>',get_option('theme_company_address')); ?>
	                        <a href="http://maps.google.com/maps?q=<?php echo urlencode(get_option('theme_company_address'));?>" target="_blank">
	                            Plan uw route
	                        </a>
	                    </div>
					</div>
				</div>
			</div>

			<div class="maps">
			    <div id="glmap">
			        <div id="map-canvas"></div>
			        <input type="hidden" value="<?php echo get_option('theme_company_address') ?>" id="address">
			        <input type="hidden" value="<?php bloginfo('stylesheet_directory') ?>" id="styles">
			        <script src="<?php bloginfo('stylesheet_directory') ?>/assets/js/init.maps.js"></script>
			    </div>
			</div>

			<div class="kvkdata">
				<div class="row">
					<div class="small-6 column">
						<div class="element">
							<label>IBAN</label>
							<span><?php echo get_option('theme_company_iban_number'); ?></span>
						</div>
					</div>
					<div class="small-6 column">
						<div class="element">
							<label>KVK</label>
							<span><?php echo get_option('theme_company_commerce_number'); ?></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="small-6 column">
						<div class="element">
							<label>BIC</label>
							<span><?php echo get_option('theme_company_bic_number'); ?></span>
						</div>
					</div>
					<div class="small-6 column">
						<div class="element">
							<label>BTW</label>
							<span><?php echo get_option('theme_company_tax_number'); ?></span>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="small-12 large-6 column">
			<?php echo $this->partial('partials/page/singular-simple.phtml') ?>
		</div>
	</div>
</section>

<section class="crumbs">
	<div class="row">
		<div class="small-12 column center">
			<?php echo $this->breadcrumbs() ?>
		</div>
	</div>
</section>

<?php echo $this->partial('partials/layout/pagebottom.phtml') ?>
