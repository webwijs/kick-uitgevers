<?php
/**
 * Template Name: Homepage
 * Layouts: home
 */
?>
<?php the_post(); ?>

<section class="homecontent">
    <?php $image = wp_get_attachment_image_src(get_option('theme_thumbnail_video'), 'homevideo', false ); ?>
    <div class="videoimage show-for-large" style="background: url(<?php echo $image[0]; ?>) no-repeat center center / cover;"></div>

    <div class="row">
        <div class="small-12 large-6 columns relative">
            <?php echo $this->partial('partials/page/singular.phtml') ?>
        </div>
        <div class="large-6 columns relative">
            <div class="videoimage full show-for-small hide-for-large" style="background: url(<?php echo $image[0]; ?>) no-repeat center center / cover;"></div>
            <div class="videotext">
                <?php if($videoid = get_option('theme_video_id')): ?>
                    <a href="#videopopup" class="videobtn toggle"></a>
                <?php endif; ?>
                <h4><?php echo get_post_meta(get_the_ID(),'_text_videosubtitle', true); ?></h4>
                <h3><?php echo get_post_meta(get_the_ID(),'_text_videotitle', true); ?></h3>
            </div>
        </div>
    </div>
</section>
    
<section class="soon-news">
    <div class="row">

        <div class="small-12 large-6 columns">
            <?php echo $this->sidebarArea('content-soon') ?>
        </div>

        <div class="small-12 large-6 columns relative greybg">
            <div class="content-news">
                <h4>Actueel</h4>
                <?php 
                    $defaults = [
                        'template' => 'partials/news/home-list.phtml',
                        'queryArgs' => [
                            'posts_per_page' => 1,
                            'nopaging' => false
                        ]
                    ];
                    echo $this->listNews($defaults); 
                ?>
            </div>
        </div>

    </div>
</section>

<section class="usps">
    <div class="row">
        <?php echo $this->sidebarArea('content-usps') ?>
    </div>
</section>

<section class="available">
    <div class="row">
        <div class="small-12 columns">
            <h2 class="sectiontitle">Nu verkrijgbaar</h2>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <?php echo do_shortcode( '[recent_products per_page="4" columns="4"]' ); ?>
        </div>
    </div>
</section>

<section class="also">
    <div class="row">
        <div class="small-12 columns">
            <h2 class="sectiontitle">Ook door ons uitgegeven</h2>
        </div>
    </div>
    <div class="row">
        <div class="small-12 large-3 columns relative">
            <div class="tag">
                <?php echo $this->partial('partials/svg/logoback.svg'); ?>
                <span>Top<strong>3</strong></span>
            </div>
            <?php echo $this->sidebarArea('content-top3') ?>
        </div>
        <div class="small-12 large-9 columns">
            <?php echo do_shortcode( '[featured_products per_page="3" columns="3"]' ); ?>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns center">
            <a href="<?php echo get_permalink( wc_get_page_id('shop') ); ?>" class="btn arrow redborder darktext">Ontdek al onze boeken</a>
        </div>
    </div>
</section>

