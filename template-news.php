<?php
/**
 * Template Name: News
 * Layouts: news
 */
?>

<?php the_post(); ?>

<?php 
    $this->facetedSearch()->init(array(
        'type' => 'News',
        'template' => 'partials/news/faceted-search.phtml',
        'vars' => array(
            'list-item' => 'partials/news/list-item.phtml',
            'not-found' => 'partials/news/not-found.phtml'
        )
    ));
?>

<section class="pagetop">
	<div class="row">
		<div class="small-12 column center">
			<h3 class="subtitle red"><?php the_title() ?></h3>
			<h1><?php echo get_post_meta(get_the_ID(),'_text_toptitle',true); ?></h1>
		</div>
	</div>
</section>

<section class="news page-<?php echo get_post_type() ?>">
	<?php echo $this->facetedSearch(); ?>
</section>

<section class="crumbs">
	<div class="row">
		<div class="small-12 column center">
			<?php echo $this->breadcrumbs() ?>
		</div>
	</div>
</section>

<?php echo $this->partial('partials/layout/pagebottom.phtml') ?>
