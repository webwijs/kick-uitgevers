<?php
/*
 * Layouts: two-columns-left
 */
 /**
  * @todo : If you want to change the WooCommerce templates themselves, go to the
  * WooCommerce plugin folder and copy the 'templates' folder to the theme folder
  * and rename it to 'woocommerce'.
  */
?>

<?php if(is_single()): ?>
	<?php echo $this->partial('partials/woocommerce/single.phtml') ?>
<?php else: ?>
	<?php echo $this->partial('partials/woocommerce/category.phtml') ?>
<?php endif; ?>



<section class="crumbs pre-footer">
	<div class="row">
		<div class="small-12 column center">
			<?php echo $this->breadcrumbs() ?>
		</div>
	</div>
</section>
